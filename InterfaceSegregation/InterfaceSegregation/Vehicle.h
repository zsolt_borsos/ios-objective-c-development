//
//  Vehicle.h
//  InterfaceSegregation
//
//  Created by Andrew Muncey on 02/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Vehicle <NSObject>

-(void)startEngine;
-(void)accelerate;
-(void)brake;


@end
