//
//  Features.h
//  InterfaceSegregation
//
//  Created by Zsolt Borsos on 12/01/2015.
//  Copyright (c) 2015 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Features <NSObject>

-(void)turnOnRadio;
-(void)turnOffRadio;
-(void)playCD;
-(void)ejectCD;

@end
