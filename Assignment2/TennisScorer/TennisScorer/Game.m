#import "Game.h"
@implementation Game{
    int p1Points, p2Points;
}

//points are as follows:
// 0 = 0
//15 = 1
//30 = 2
//40 = 3
// A = 4

-(void)addPointToPlayer1{p1Points == 3 && p2Points == 4 ? p2Points-- : p1Points++;}

-(void)addPointToPlayer2{p2Points == 3 && p1Points == 4 ? p1Points-- : p2Points++;}

-(NSString *)player1Score{return ![self gameComplete] ? [self scoreFromPoints:p1Points] : @"";}

-(NSString *)player2Score{return ![self gameComplete] ? [self scoreFromPoints:p2Points] : @"";}

-(BOOL)player1Won{return (p1Points == 5 || (p1Points == 4 && p2Points <=2)) ? YES : NO;}

-(BOOL)player2Won{return (p2Points == 5 || (p2Points == 4 && p1Points <=2)) ? YES: NO;}

-(BOOL)gameComplete{return ([self player1Won] || [self player2Won]);}

-(NSString *)scoreFromPoints:(int)points{
    return [[NSArray arrayWithObjects:@"0",@"15",@"30",@"40",@"A",@"",nil] objectAtIndex:points];}

-(int)gamePointsForPlayer1{return p1Points>=3 ? MAX(p1Points-p2Points, 0) : 0;}

-(int)gamePointsForPlayer2{return p2Points>=3 ? MAX(p2Points-p1Points, 0) : 0;}

@end
