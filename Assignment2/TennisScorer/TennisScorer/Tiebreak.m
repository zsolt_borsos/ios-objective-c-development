//
//  Tiebreak.m
//  TennisScorer
//
//  Created by Zsolt Borsos on 11/05/2015.
//  Copyright (c) 2015 University of Chester. All rights reserved.
//

#import "Tiebreak.h"

@implementation Tiebreak{
    int p1Points, p2Points;
}


-(void)addPointToPlayer1{p1Points++;}

-(void)addPointToPlayer2{p2Points++;}

-(NSString *)player1Score{return [NSString stringWithFormat:@"%d", p1Points];}

-(NSString *)player2Score{return [NSString stringWithFormat:@"%d", p2Points];}

-(BOOL)player1Won{

    if (p1Points >= 7 && (p1Points > p2Points+1)) {
        return YES;
    }else{
        return NO;
    }
}

-(BOOL)player2Won{
    if (p2Points >= 7 && (p2Points > p1Points+1)) {
        return YES;
    }else{
        return NO;
    }
}

-(BOOL)gameComplete{return ([self player1Won] || [self player2Won]);}

-(int)gamePointsForPlayer1{return p1Points>=6 ? MAX(p1Points-p2Points, 0) : 0;}

-(int)gamePointsForPlayer2{return p2Points>=6 ? MAX(p2Points-p1Points, 0) : 0;}


@end
