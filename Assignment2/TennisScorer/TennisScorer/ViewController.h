//
//  ViewController.h
//  TennisScorer
//
//  Created by Andrew Muncey on 17/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Match.h"

@interface ViewController : UIViewController <MatchDelegate>


@property (strong, nonatomic) IBOutlet UILabel *p1Name;
@property (strong, nonatomic) IBOutlet UILabel *p2Name;

@property (strong, nonatomic) IBOutlet UILabel *p1CurrentPoints;
@property (strong, nonatomic) IBOutlet UILabel *p2CurrentPoints;
@property (strong, nonatomic) IBOutlet UILabel *p1CurrentGames;
@property (strong, nonatomic) IBOutlet UILabel *p2CurrentGames;
@property (strong, nonatomic) IBOutlet UILabel *p1PreviousSets;
@property (strong, nonatomic) IBOutlet UILabel *p2PreviousSets;

- (IBAction)p1PointPressed:(id)sender;
- (IBAction)p2PointPressed:(id)sender;
- (IBAction)restartPressed:(id)sender;

@end
