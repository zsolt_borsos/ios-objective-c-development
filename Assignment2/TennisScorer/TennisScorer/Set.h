//
//  Set.h
//  TennisScorer
//
//  Created by Zsolt Borsos on 27/04/2015.
//  Copyright (c) 2015 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Set : NSObject

@property int p1Score;
@property int p2Score;
//@property BOOL p1Serving;
@property BOOL isTiebreak;

//determine if the set is finished
-(BOOL)finished;

//determine if a tiebreak needs playing
-(BOOL)tiebreak;


//get the winner's number
-(int)getWinnerPlayerNumber;

@end
