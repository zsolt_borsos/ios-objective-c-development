//
//  Game.h
//  Tennis
//
//  Created by Andrew Muncey.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Game : NSObject

-(void)addPointToPlayer1;
-(void)addPointToPlayer2;

/**
 Returns player 1's score for a game in progress.
An empty string will be returned for completed games.
 @return An NSString with a value of either 0, 15, 30, 40 or A.
 */
-(NSString *)player1Score;

/**
 Returns player 2's score for a game in progress.
 An empty string will be returned for completed games.
 @return An NSString with a value of either 0, 15, 30, 40 or A.
 */
-(NSString *)player2Score;

/**
 Determines if player 1 has won the game.
 @return YES if player 1 has one, NO if they have not won or the game is still in progress.
 */
-(BOOL)player1Won;

/**
 Determines if player 2 has won the game.
 @return YES if player 2 has one, NO if they have not won or the game is still in progress.
 */
-(BOOL)player2Won;

/**
 Determines if the game is complete.
 @return YES if the game is complete, NO if it is in progress.
 */
-(BOOL)gameComplete;

/**
 Returns the number of game points player 1 has (e.g. they only need one point to win)
 Example: Player 1 has 40, player 2 has 15, player 1 needs 1 point to win, but player 2 would need another 2 points to catch up, so player 1 has 2 game points.
 @return an int value of how many game points player 1 has
 */
-(int)gamePointsForPlayer1;

/**
 Returns the number of game points player 2 has (e.g. they only need one point to win)
 Example: Player 2 has 40, player 1 has 15, player 2 needs 1 point to win, but player 1 would need another 2 points to catch up, so player 2 has 2 game points.
 @return an int value of how many game points player 2 has
 */
-(int)gamePointsForPlayer2;

@end
