//
//  AppDelegate.h
//  TennisScorer
//
//  Created by Andrew Muncey on 17/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
