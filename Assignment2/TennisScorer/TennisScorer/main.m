//
//  main.m
//  TennisScorer
//
//  Created by Andrew Muncey.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
