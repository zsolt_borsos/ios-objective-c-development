//
//  Set.m
//  TennisScorer
//
//  Created by Zsolt Borsos on 27/04/2015.
//  Copyright (c) 2015 University of Chester. All rights reserved.
//

#import "Set.h"

@implementation Set{
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _isTiebreak = NO;
        _p1Score = 0;
        _p2Score = 0;
    }
    return self;
}



-(BOOL)finished {
    
    if (_isTiebreak) {
        if (_p1Score >= 7 && (_p1Score > _p2Score+1) ) {
            return YES;
        }
        if (_p2Score >= 7 && (_p2Score > _p1Score+1 )) {
            return YES;
        }
        return NO;
    }else{
        if (_p1Score >= 6 || _p2Score >= 6) {
            if (_p1Score >= _p2Score + 2) {
                return YES;
            }
            if (_p2Score >= _p1Score + 2) {
                return YES;
            }
        }
        return NO;
    }
    
}

-(BOOL)tiebreak {
    
    if (_p1Score == 6 && _p1Score == _p2Score) {
        return YES;
    }else{
        return NO;
    }
}



-(int)getWinnerPlayerNumber {
    
    if (_p1Score > _p2Score) {
        return 1;
    }else{
        return 2;
    }
    
}

@end
