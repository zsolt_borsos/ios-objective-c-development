//
//  Match.h
//  TennisScorer
//
//  Created by Zsolt Borsos on 27/04/2015.
//  Copyright (c) 2015 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Set.h"
#import "Game.h"
#import "Tiebreak.h"

@protocol MatchDelegate <NSObject>

-(void)updateGameScoreDisplay;
-(void)gameIsFinished;
-(void)setIsFinishedWithTiebreak:(BOOL)tiebreak;
-(void)matchIsFinished;


@end

@interface Match : NSObject

//current set
@property Set *currentSet;

//current game
@property Game *currentGame;

@property int currentServer;

@property int p1SetsWon;
@property int p2SetsWon;

@property id<MatchDelegate>delegate;

//add score to a player in the current game
-(void)addScoreToPlayer:(int)playerNumber;

-(BOOL)allowTiebreak;

-(NSString *)createStatsText;

-(BOOL)matchFinished;


@end
