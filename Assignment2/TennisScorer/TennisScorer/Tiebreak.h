//
//  Tiebreak.h
//  TennisScorer
//
//  Created by Zsolt Borsos on 11/05/2015.
//  Copyright (c) 2015 University of Chester. All rights reserved.
//

#import "Game.h"

@interface Tiebreak : Game


-(void)addPointToPlayer1;
-(void)addPointToPlayer2;

/**
 Returns player 1's score for a game in progress.
 An empty string will be returned for completed games.
 @return An NSString with the score
 */
-(NSString *)player1Score;

/**
 Returns player 2's score for a game in progress.
 An empty string will be returned for completed games.
 @return An NSString with the score
 */
-(NSString *)player2Score;

/**
 Determines if player 1 has won the game.
 @return YES if player 1 has one, NO if they have not won or the game is still in progress.
 */
-(BOOL)player1Won;

/**
 Determines if player 2 has won the game.
 @return YES if player 2 has one, NO if they have not won or the game is still in progress.
 */
-(BOOL)player2Won;

/**
 Determines if the game is complete.
 @return YES if the game is complete, NO if it is in progress.
 */
-(BOOL)gameComplete;

-(int)gamePointsForPlayer1;
-(int)gamePointsForPlayer2;



@end
