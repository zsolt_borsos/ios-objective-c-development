//
//  ViewController.m
//  TennisScorer
//
//  Created by Andrew Muncey on 17/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "ViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <AVFoundation/AVFoundation.h>


@interface ViewController (){
    Match *_match;
}

@end

@implementation ViewController

-(void)viewDidLoad{
    
    _match = [[Match alloc]init];
    _match.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated{

    
}

- (IBAction)p1PointPressed:(id)sender {
   //player 1 has scored a point
    if (![_match matchFinished]) {
        [_match addScoreToPlayer:1];
    }

    
}

- (IBAction)p2PointPressed:(id)sender {
  //player 2 has scored a point
    if (![_match matchFinished]) {
        [_match addScoreToPlayer:2];
    }

}

- (IBAction)restartPressed:(id)sender {
    //restart the tennis match
    _match = NULL;
    _match = [[Match alloc]init];
    _match.delegate = self;
    [self resetDisplay];
}




-(void)resetDisplay {
    
    _p1CurrentPoints.text = @"-";
    _p2CurrentPoints.text = @"-";
    _p1PreviousSets.text = @"-";
    _p2PreviousSets.text = @"-";
    _p1CurrentGames.text = @"-";
    _p2CurrentGames.text = @"-";
}


//delegate methods
-(void)updateGameScoreDisplay {
    _p1CurrentPoints.text = [_match.currentGame player1Score];
    
    
    _p1CurrentGames.text = [NSString stringWithFormat:@"%d [%d]", _match.currentSet.p1Score, [_match.currentGame gamePointsForPlayer1]];
    
    
    
    //_p1CurrentGames.text = [NSString stringWithFormat:@"%d",_match.currentSet.p1Score];
    //_p2CurrentGames.text = [NSString stringWithFormat:@"%d",_match.currentSet.p2Score];
    
    _p2CurrentPoints.text = [_match.currentGame player2Score];
    
    _p2CurrentGames.text = [NSString stringWithFormat:@"%d [%d]", _match.currentSet.p2Score, [_match.currentGame gamePointsForPlayer2]];
    
    if (_match.currentServer == 1) {
        _p1Name.backgroundColor = [UIColor yellowColor];
        _p2Name.backgroundColor = [UIColor whiteColor];
    }else{
        _p2Name.backgroundColor = [UIColor yellowColor];
        _p1Name.backgroundColor = [UIColor whiteColor];
    }
    
}


//This method handling the alert window that appear when the match is finished.
//it also handles the user account/twitter stuff
-(void)matchIsFinished {
    //get the results from the match
    NSString *winText = [_match createStatsText];
    

    //create a new alert
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Match finished"
                                                                   message: winText
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    //add default button
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    //add tweet button
    UIAlertAction* tweetAction = [UIAlertAction actionWithTitle:@"Tweet" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

        //check twitter acc availability
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            NSLog(@"Twitter is ready to roll!");
            //get acc store
            ACAccountStore *acStore = [[ACAccountStore alloc]init];
            //get twitter acc type?
            ACAccountType *twitterAccType = [acStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
            
            //ask user for access!
            [acStore requestAccessToAccountsWithType:twitterAccType options:nil completion:^(BOOL granted, NSError *error) {
                if (granted) {
                    //NSLog(@"Got access!");
                    
                    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
                    
                    //get the accounts in an array
                    NSArray *accounts = [acStore accountsWithAccountType:twitterAccType];
                    
                    //create params for the request
                    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
                    
                    //add status key with the object of the text generated from the match results
                    [params setObject:winText forKey:@"status"];
                    
                    //create request object
                    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:url parameters:params];
                    
                    
                    request.account = [accounts lastObject];
                    //[request setAccount:[accounts lastObject]];
                    
                    //perform the request with handler
                    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                        //read/transform response
                        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                        
                        //check for status
                        if ([[response objectForKey:@"Status"] isEqualToString:@"200 OK"]) {
                            NSLog(@"Tweeted OK!");
                        }else{
                            NSLog(@"Something went wrong?");
                            NSLog(@"Error: %@", error);
                        }

                        //NSLog(@"%@", [response description]);
                        
                        //NSLog(@"%@", urlResponse);
                    }];
                    
                }else{
                    NSLog(@"Did not get the access to the twitter acc from user.");
                }
            }];
            
            
        }else{
            NSLog(@"Twitter acc not found on device!");
        }
        
    }];
    
    
    //add actions to the alert window
    [alert addAction:defaultAction];
    [alert addAction:tweetAction];
    
    //present the alert
    [self presentViewController:alert animated:YES completion:nil];
    
    

}


-(void)gameIsFinished {
    _p1CurrentGames.text = [NSString stringWithFormat:@"%d",_match.currentSet.p1Score];
    _p2CurrentGames.text = [NSString stringWithFormat:@"%d",_match.currentSet.p2Score];
    
}

-(void)setIsFinishedWithTiebreak:(BOOL)tiebreak {

    //if the display is on default (assuming if p1 is default then p2 should be on default too)
    //then clear the value so we can append to it later without extra work
    if ([_p1PreviousSets.text isEqualToString:@"-"]) {
        
        _p1PreviousSets.text = @"";
        _p2PreviousSets.text = @"";
        
    }
    if (tiebreak) {
        
        if (_match.currentGame.player1Won) {
            //if there are previous set scores then append the value
            _p1PreviousSets.text = [_p1PreviousSets.text stringByAppendingString:@"7"];
            
            _p2PreviousSets.text = [_p2PreviousSets.text stringByAppendingString:@"6"];
        }else{
            _p1PreviousSets.text = [_p1PreviousSets.text stringByAppendingString:@"6"];
            
            _p2PreviousSets.text = [_p2PreviousSets.text stringByAppendingString:@"7"];
        }
    }else{
        
        //append the value
        _p1PreviousSets.text = [_p1PreviousSets.text stringByAppendingString:[NSString stringWithFormat:@"%d", _match.currentSet.p1Score]];
        
        _p2PreviousSets.text = [_p2PreviousSets.text stringByAppendingString:[NSString stringWithFormat:@"%d", _match.currentSet.p2Score]];

    }
    
    AVSpeechSynthesizer *synth = [[AVSpeechSynthesizer alloc] init];
    
    NSMutableString *textToSay = [[NSMutableString alloc]initWithString:@"Set finished, with, "];
    [textToSay appendString:[NSString stringWithFormat:@"%d, ",_match.currentSet.p1Score]];
    [textToSay appendString:[NSString stringWithFormat:@"%d ",_match.currentSet.p2Score]];
    AVSpeechUtterance *sayIt = [AVSpeechUtterance speechUtteranceWithString:textToSay];
    [synth speakUtterance:sayIt];
    
    _p1CurrentGames.text = @"0";
    _p2CurrentGames.text = @"0";
    
}



@end
