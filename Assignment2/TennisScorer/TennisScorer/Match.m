//
//  Match.m
//  TennisScorer
//
//  Created by Zsolt Borsos on 27/04/2015.
//  Copyright (c) 2015 University of Chester. All rights reserved.
//

#import "Match.h"
#import <AVFoundation/AVFoundation.h>


@implementation Match{
    
    BOOL _finishedGame;
    BOOL _finishedSet;
    NSMutableArray *_sets;
    BOOL _tiebreakStarted;
    BOOL _tiebreakFinished;
    int _tiebreakServeCount;
    //int _p1SetsWon;
    //int _p2SetsWon;
    AVAudioPlayer *_player;
    
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        //a list for sets
        _sets = [[NSMutableArray alloc]init];
        
        //current game and set
        _currentGame = [[Game alloc]init];
        _currentSet = [[Set alloc]init];
        _currentServer = 1;
        
        _tiebreakServeCount = 0;
        _tiebreakStarted = NO;
        _tiebreakFinished = NO;
        
        //sound
        NSString *path = [[NSBundle mainBundle] pathForResource:@"tick"
                                                         ofType:@"wav"];
        NSURL *soundFileURL = [NSURL fileURLWithPath:path];
        
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL
                                                                       error:nil];
        //_player.numberOfLoops = 0;
        
        
        
    }
    return self;
}


//method to check if we are on the last set
-(BOOL)allowTiebreak {
    
    int setsInMatch = [_sets count];
    if (setsInMatch == 4) {
        return NO;
    }else{
        return YES;
    }
    
}


-(void)addScoreToPlayer:(int)playerNumber {
    
    NSLog(@"Adding score to: %d", playerNumber);
    
    //check for tiebreak 1st as this will determine how the scoring is
    if ([_currentSet tiebreak]) {
        
        //the set object sent the signal for the tiebreak
        //we need to check the status of the current match to decide if we allowed to do the tiebreak or not
        
        if ([self allowTiebreak]) {
            
            //if the tiebreak is not started then start it
            if (!_tiebreakStarted) {
                //a flag for us to track the status of the tiebreak
                _tiebreakStarted = YES;
                _currentSet.isTiebreak = YES;
                _currentGame = [[Tiebreak alloc]init];
                //[self changeServer];
                
            }
            
        }
        
    }
    
    //select scoring method
    if (_tiebreakStarted) {
        [self tiebreakScoring:playerNumber];
    }else{
        [self normalScoring:playerNumber];
    }
    
    
    //check if set is finished
    if ([_currentSet finished]) {
        [self.delegate setIsFinishedWithTiebreak:NO];
        [_sets addObject:_currentSet];
        
        
        //not sure if we need this, but we keep track of set scores here too
        if ([_currentSet getWinnerPlayerNumber] == 1) {
            _p1SetsWon++;
        }else {
            _p2SetsWon++;
        }
        
        
        
        //reset current set
        _currentSet = NULL;
        
        //create a new set
        _currentSet = [[Set alloc]init];
         
    }
    
    
    //check if match is finished
    
    if ([self matchFinished]) {
        NSLog(@"We are finished here!");
        
        //do a popup for displaying stats
        //stop interaction/only allow resetting
        [self.delegate matchIsFinished];
        
    }
    
    
}


-(void)tiebreakScoring:(int)playerNumber {
    
    switch (playerNumber) {
        case 1:
            [_currentGame addPointToPlayer1];
            break;
        case 2:
            [_currentGame addPointToPlayer2];
            break;
        default:
            NSLog(@"Wrong player number...");
            break;
    }
    
    if ([_currentGame gameComplete]) {
        if ([_currentGame player1Won]) {
            _currentSet.p1Score++;
        }else{
            _currentSet.p2Score++;
        }
        [self.delegate setIsFinishedWithTiebreak:YES];
        
        _currentGame = NULL;
        _currentGame = [[Game alloc]init];
        
        _tiebreakStarted = NO;
        _currentSet.isTiebreak = NO;
        _tiebreakFinished = YES;

        [_sets addObject:_currentSet];
        
        //not sure if we need this, but we keep track of set scores here too
        if ([_currentSet getWinnerPlayerNumber] == 1) {
            _p1SetsWon++;
        }else {
            _p2SetsWon++;
        }
        
        //reset current set
        _currentSet = NULL;
        
        //create a new set
        _currentSet = [[Set alloc]init];
        
        _tiebreakServeCount = 0;
        
        return;

    }

    //1st time serve once
    if (_tiebreakServeCount == 0) {
        [self changeServer];
        
        
    }
    
    //then alternate after every 2 points
    if (_tiebreakServeCount > 0 && ((_tiebreakServeCount % 2) == 0) ) {
        [self changeServer];
    }
    
    _tiebreakServeCount++;
    
    [self.delegate updateGameScoreDisplay];

}

-(void)changeServer {
   
    if (_currentServer == 1) {
        _currentServer = 2;
    }else{
        _currentServer = 1;
    }
    
    // Play the sound
    
    [_player play];

}



-(void)normalScoring:(int)playerNumber {
    switch (playerNumber) {
        case 1:
            [_currentGame addPointToPlayer1];
            break;
        case 2:
            [_currentGame addPointToPlayer2];
            break;
        default:
            NSLog(@"Wrong player number...");
            break;
    }
    
    if ([_currentGame gameComplete]) {
        if ([_currentGame player1Won]) {
            _currentSet.p1Score++;
        }else{
            _currentSet.p2Score++;
        }
        [self.delegate gameIsFinished];
        _currentGame = NULL;
        _currentGame = [[Game alloc]init];
        
        if (![_currentSet tiebreak]) {
            [self changeServer];
        }
        
    }
    
    [self.delegate updateGameScoreDisplay];
}



-(BOOL)matchFinished {
    
    if (_p1SetsWon >= 3 && (_p2SetsWon < _p1SetsWon +1)) {
        return YES;
    }
    
    if (_p2SetsWon >= 3 && (_p1SetsWon < _p2SetsWon +1)) {
        return YES;
    }
    
    return NO;
}



-(NSString *)createStatsText{
    
    NSMutableString *text = [[NSMutableString alloc]initWithString:@"The match is finished."];

    if (_p1SetsWon > _p2SetsWon) {
        [text appendString:@" Player1 won!"];
    }else{
        [text appendString:@" Player2 won!"];
    }

    [text appendString:@"\nThe score is:\nPlayer1:\t"];
    
    for (Set *set in _sets) {
        [text appendString: [NSString stringWithFormat:@"%d", set.p1Score]];
    }
    
    [text appendString:@"\nPlayer2:\t"];
    
    for (Set *set in _sets) {
        [text appendString: [NSString stringWithFormat:@"%d", set.p2Score]];
    }
    
    return text;
}


@end
