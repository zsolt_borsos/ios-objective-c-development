//
//  TennisScorerTests.m
//  TennisScorerTests
//
//  Created by Andrew Muncey.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Game.h"
#import "Match.h"
#import "Tiebreak.h"
#import "Set.h"


@interface TennisScorerTests : XCTestCase

@end

@implementation TennisScorerTests{
    Game *_game;
    Match *_match;
    Tiebreak *_tiebreak;
    Set *_set;
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _game = [[Game alloc]init];
    _match = [[Match alloc]init];
    _tiebreak = [[Tiebreak alloc]init];
    _set = [[Set alloc]init];
    
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//Game class tests
//not writing more test on the game class as it was already created.
-(void)testReachesDeuce{
    
    [_game addPointToPlayer1];
    [_game addPointToPlayer1];
    [_game addPointToPlayer1];
    [_game addPointToPlayer2];
    [_game addPointToPlayer2];
    [_game addPointToPlayer2];
    XCTAssertTrue([[_game player1Score] isEqualToString:@"40"], @"p1 at 40");
}


-(void)testGameIsCompleteAfterPlayer1Scores4ConsecutivePoints{
 
    [_game addPointToPlayer1];
    [_game addPointToPlayer1];
    [_game addPointToPlayer1];
    [_game addPointToPlayer1];
    XCTAssertTrue([_game player1Won], @"P1Wins OK");
}



//tiebreak class tests

-(void)testPlayer1WinsTiebreak {
    
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    XCTAssertTrue([_tiebreak player1Won], @"P1 wins the tiebreak");
    
}

-(void)testPlayer2WinsTiebreakWith2points {
    
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    XCTAssertTrue([_tiebreak player2Won], @"P2 wins the tiebreak");
}

-(void)testPlayer1Have6BreakPoints {
    
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    XCTAssertTrue(([_tiebreak gamePointsForPlayer1] == 6), @"P1 has 6 break points!");
    
}

-(void)testPlayer2Have4BreakPoints {
    
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    XCTAssertTrue(([_tiebreak gamePointsForPlayer2] == 4), @"P2 has 4 break points!");
    
}

-(void)testTiebreakGameIsnotFinished {
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer1];
    [_tiebreak addPointToPlayer2];
    [_tiebreak addPointToPlayer2];
    XCTAssertFalse([_tiebreak gameComplete], @"Tiebreak is not finished just yet!");
}

//set class tests

-(void)testSetClassNoticedTiebreak {
    
    _set.p1Score = 6;
    _set.p2Score = 6;
    XCTAssertTrue([_set tiebreak], @"Set noticed tiebreak correctly!");
}

-(void)testSetNotFalslyNoticingTiebreak {
    
    _set.p1Score = 4;
    _set.p2Score = 6;
    XCTAssertFalse([_set tiebreak], @"Set did not notice tiebreak, correctly!");
}

-(void)testSetIsFinished {
    
    _set.p1Score = 2;
    _set.p2Score = 6;
    XCTAssertTrue([_set finished], @"Set is finished.");
    
}

-(void)testSetIsNotFinishedIfItIsTiebreak {
    
    _set.p1Score = 2;
    _set.p2Score = 6;
    _set.isTiebreak = YES;
    XCTAssertFalse([_set finished], @"Set is not yet finished if it is a tiebreak.");
    
}


//match class tests

-(void)testMatchCurrentSetIsAnInstanceOfSetClass {
    XCTAssertTrue([[_match currentSet]isMemberOfClass:[Set class]], @"Set is the right class type.");
}

-(void)testMatchCurrentGameIsAnInstanceOfGameClass {
    XCTAssertTrue([[_match currentGame]isMemberOfClass:[Game class]], @"Game is the right class type.");
}

-(void)testMatchCurrentGameIsAnInstanceOfTiebreakClass {
    
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:1];
    }
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:2];
    }

    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    
    //we should be in a tiebreak game now
    [_match addScoreToPlayer:2];
    
    XCTAssertTrue([[_match currentGame]isMemberOfClass:[Tiebreak class]], @"Game is the right class type (tiebreak).");
}


-(void)testTiebreakScoreIsDisplayedCorrectly {
    
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:1];
    }
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:2];
    }
    
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    
    //we should be in a tiebreak game now
    [_match addScoreToPlayer:2];
    
    XCTAssertTrue([[_match.currentGame player2Score] isEqualToString:@"1"], @"Tiebreak score is displayed correctly.");
}

-(void)testTiebreakIsAllowedWhenNotOnLastSet {
    
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:1];
    }
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:2];
    }
    
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    
    XCTAssertTrue([_match allowTiebreak], @"Tiebreak is allowed correctly.");
    
}

-(void)testTiebreakIsNoticedInFirstSet {
    
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:1];
    }
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:2];
    }
    
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    
    XCTAssertTrue([_match.currentSet tiebreak], @"Tiebreak is noticed in current set correctly.");
    
}

-(void)testTiebreakIsNoticedInSecondSet {
    
    for (int i = 0; i < 24; i++) {
        [_match addScoreToPlayer:1];
    }

    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:1];
    }

    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:2];
    }
    
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    [_match addScoreToPlayer:2];
    
    XCTAssertTrue([_match.currentSet tiebreak], @"Tiebreak is noticed in second set correctly.");
    
}

-(void)testSetScoreIsCorrectWith2 {
    for (int i = 0; i < 48; i++) {
        [_match addScoreToPlayer:1];
    }
    
    XCTAssertTrue(_match.p1SetsWon == 2, @"Set score is correct.");
}

-(void)testSetScoreIsCorrectWith3To2 {
    
    //p1 win 2 sets
    for (int i = 0; i < 48; i++) {
        [_match addScoreToPlayer:1];
    }
    
    //p2 win 3 sets
    for (int i = 0; i < 72; i++) {
        [_match addScoreToPlayer:2];
    }
    
    XCTAssertTrue((_match.p1SetsWon == 2 && _match.p2SetsWon == 3 ), @"Set score is correct.");
}


-(void)testTiebreakIsNotAllowedInLastSet {
    
    //p1 win 2 sets
    for (int i = 0; i < 48; i++) {
        [_match addScoreToPlayer:1];
    }
    //p2 win 2 sets
    for (int i = 0; i < 48; i++) {
        [_match addScoreToPlayer:2];
    }
    
    //progressing on 5th set
    
    //win 5 games
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:1];
    }
    
    //win 5 games
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:2];
    }
    
    XCTAssertTrue(_match.p2SetsWon == 2, @"P2 on 2 sets.");
    XCTAssertTrue(_match.p1SetsWon == 2, @"P1 on 2 sets.");
    XCTAssertFalse([_match allowTiebreak], @"Tiebreak is not allowed in fifth set.");
    
}

-(void)testPlayer1StartsServingAtStart {
    
    [_match addScoreToPlayer:1];
    
    XCTAssertTrue(_match.currentServer == 1, @"P1 is serving.");
    
}

-(void)testPlayer2ServersOn2ndGame {
    
    
    for (int i = 0; i < 5; i++) {
        [_match addScoreToPlayer:1];
    }
    XCTAssertTrue(_match.currentServer == 2, @"P2 is serving on 2nd game.");
}


-(void)testTiebreakServingCorrectlyStarts {
    
    //win 5 games
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:1];
    }
    
    //win 5 games
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:2];
    }
    
    for (int i = 0; i < 4; i++) {
        [_match addScoreToPlayer:1];
    }
    
    //win 5 games
    for (int i = 0; i < 4; i++) {
        [_match addScoreToPlayer:2];
    }
    XCTAssertTrue([_match.currentSet tiebreak], @"Tiebreak is noted in set.");
    XCTAssertTrue([_match allowTiebreak], @"Tiebreak is allowed.");
    
    //the player that served before the tiebreak should start serving in the tiebreak
    XCTAssertTrue(_match.currentServer == 2, @"P2 is serving.");
}

-(void)testTiebreakServingCorrectlyContinues {
    
    //win 5 games
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:1];
    }
    
    //win 5 games
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:2];
    }
    
    for (int i = 0; i < 4; i++) {
        [_match addScoreToPlayer:1];
    }
    
    //win 5 games
    for (int i = 0; i < 4; i++) {
        [_match addScoreToPlayer:2];
    }
    
    
    //after the 1st serve the next player serves twice
    XCTAssertTrue(_match.currentServer == 2, @"P2 is serving.");
}

-(void)testTiebreakServingCorrectlyContinuesFurther {
    
    //win 5 games
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:1];
    }
    
    //win 5 games
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:2];
    }
    
    for (int i = 0; i < 4; i++) {
        [_match addScoreToPlayer:1];
    }
    
    //win 5 games
    for (int i = 0; i < 4; i++) {
        [_match addScoreToPlayer:2];
    }
    
    [_match addScoreToPlayer:1];
    
    //players serve 2 each...
    XCTAssertTrue(_match.currentServer == 1, @"P1 is serving.");
}

-(void)testTiebreakServingCorrectlyContinuesEvenFurther {
    
    //win 5 games
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:1];
    }
    
    //win 5 games
    for (int i = 0; i < 20; i++) {
        [_match addScoreToPlayer:2];
    }
    
    for (int i = 0; i < 4; i++) {
        [_match addScoreToPlayer:1];
    }
    
    //win 5 games
    for (int i = 0; i < 4; i++) {
        [_match addScoreToPlayer:2];
    }
    
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    [_match addScoreToPlayer:1];
    
    //players serve 2 each...
    XCTAssertTrue(_match.currentServer == 2, @"P2 is serving.");
}

@end
