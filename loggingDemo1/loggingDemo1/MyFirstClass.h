//
//  MyFirstClass.h
//  loggingDemo1
//
//  Created by student on 09/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyFirstClass : NSObject
    
@property (nonatomic, strong) NSString *myNameProp;

-(NSString *) writeGreeting;




@end
