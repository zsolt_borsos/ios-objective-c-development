//
//  main.m
//  loggingDemo1
//
//  Created by student on 09/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        //NSLog(@"Hello, World!");
        //NSLog(@"hi");
        
        int myInt = 15;
        NSString *myString = @"This is my string";
        float myFloatNumber = 12.546;
        
        NSLog(@"My number is: %d", myInt);
        NSLog(@"My string is: %@", myString);
        NSLog(@"My float number is: %f", myFloatNumber);
        NSLog(@"And all together... Number is %d, the float is %f and the string is %@", myInt, myFloatNumber, myString);
    }
    return 0;
}

