//
//  OfferApplicator.m
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "OfferApplicator.h"

#import "HalfPriceOffer.h"
#import "BuyOneGetOneFreeOffer.h"
#import "ThreeMeatsForTenPoundOffer.h"
#import "FreeEvianWaterWithTimesNewsPaperOffer.h"
#import "BuyTwoGetThirdFreeOffer.h"
#import "FreeSoftenerWithDetergentOffer.h"



@implementation OfferApplicator{
   NSMutableArray *_offers;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _offers = [[NSMutableArray alloc]init];
        
        //add all potential offers to an array
        [_offers addObject:[[HalfPriceOffer alloc]init] ];
        [_offers addObject:[[BuyOneGetOneFreeOffer alloc]init]];
        [_offers addObject:[[ThreeMeatsForTenPoundOffer alloc]init]];
        [_offers addObject:[[FreeEvianWaterWithTimesNewsPaperOffer alloc]init]];
        [_offers addObject:[[BuyTwoGetThirdFreeOffer alloc]init]];
        //add softener offer
        [_offers addObject:[[FreeSoftenerWithDetergentOffer alloc]init]];

        

    
    }
    return self;
}


-(NSArray*)offerDiscounts:(NSArray *)list{
    
    NSMutableArray *receiptLines = [[NSMutableArray alloc]init];
    
    //run through each offer
    for (Offer *offer in _offers) {
        if ([offer isApplicableToList:list]) {
            
            int discountvalue = [offer valueOfDiscountFromList:list];
        
            RecieptOfferLine *line = [[RecieptOfferLine alloc]init];
            line.valueInPence = discountvalue;
            line.name = offer.offerName;
            
            [receiptLines addObject:line];
        }
    }
    
    return receiptLines;
    
    //get any appliable offers and remove products from the list
    
    
    
    
}


@end
