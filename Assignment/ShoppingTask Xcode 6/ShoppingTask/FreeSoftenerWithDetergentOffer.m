//
//  FreeSoftenerWithDetergentOffer.m
//  ShoppingTask
//
//  Created by Zsolt Borsos on 23/11/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "FreeSoftenerWithDetergentOffer.h"

@implementation FreeSoftenerWithDetergentOffer{
    
    int _detergentCounter;
    int _softenerCounter;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.offerName = @"Free softener when you buy any detergent.";
        //add products to this offer
        NSArray *products = @[@17,@18,@19, @20];
        self.applicableProducts = products;
        
        _detergentCounter = 0;
        _softenerCounter = 0;
        
    }
    return self;
}


-(BOOL)isApplicableToList:(NSArray *)list{
    
    //check all items in the list
    for (ShoppingItem *item in list) {
        
        //count items in list and store them
        if ((item.productId == 17) || (item.productId == 18)) {
            _detergentCounter++;
        }else if ((item.productId == 19) || (item.productId == 20)){
            _softenerCounter++;
        }
    }
    if (_detergentCounter > 0) {
        if (_softenerCounter > 0){
            //if there is more than 1 of each item then return true
            return TRUE;
        }
    }
    //if not already returned then return false
    return FALSE;
}


-(int)valueOfDiscountFromList:(NSArray *)list{
    
    //store the list of products in offer in an array
    NSArray *productsInOffer = [[NSArray alloc] initWithArray:[self applicableProducts:list]];
    
    //sort the array
    productsInOffer = [self sortListByPrice:productsInOffer];

    //declare a variable for discount price
    int discount = 0;
    
    //store the iterations that will be used
    //to determine how many times the discount should be applied
    int iterations = 0;
    //if there are more detergents than softeners
    if (_detergentCounter > _softenerCounter) {
        //then we use the number of softeners
        iterations = _softenerCounter;
    }else{
        //otherwise we use the number of detergents
        iterations = _detergentCounter;
    }
    
    //check all products in the sorted by price list
    for (ShoppingItem *item in productsInOffer) {
        
        //if the product is a softener
        //the first ones (most expensive ones) in the array should be discounted first
        if ((item.productId == 19) || (item.productId == 20)){
            //only apply discount if iterations is more than 0
            if (iterations > 0){
                //add price to discount
                discount += item.priceInPence;
                //decrease iterations
                iterations--;
            }
        }
        
    }
    
    //return discount
    return discount;
    
    
}



@end
