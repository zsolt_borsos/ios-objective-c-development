//
//  BuyTwoGetThirdFreeOffer.m
//  ShoppingTask
//
//  Created by Andrew Muncey on 23/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "BuyTwoGetThirdFreeOffer.h"

@implementation BuyTwoGetThirdFreeOffer

//this offer should order applicable products by price and add a discount to the value of every third item

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.offerName = @"Buy Two Get Third Free on Deodorants";
        
        NSArray *products = @[@15,@16];
        self.applicableProducts = products;
    }
    return self;
}



-(BOOL)isApplicableToList:(NSArray *)list{
    
    //should return YES if a discount can be applied based on the supplied list of products (i.e. a product must be match an offer)
    
    /*
    //return true if there are more than 2 product in the list
    if ([self applicableProducts:list].count > 2) {
        return TRUE;
    }
    return FALSE;
    */
    
    //short code
    return ([self applicableProducts:list].count > 2) ? YES : NO;
}


-(int)valueOfDiscountFromList:(NSArray *)list{
    
    //order the list by price
    //discount the value of every third item in the list
    
    
    //store the list of products in offer in an array
    NSArray *productsInOffer = [[NSArray alloc] initWithArray:[self applicableProducts:list]];
    
    //sort the array 
    productsInOffer = [self sortListByPrice:productsInOffer];
    
    
    int discount = 0;
    //counter of products
    int counter = 1;
    //check all products
    for (ShoppingItem *item in productsInOffer) {
        //on every 3rd item...
        if (counter % 3 == 0) {
            //add the price to discount
            discount += item.priceInPence;
        }
        counter++;
    }
    //return the discount amount
    return discount;
    
}




@end
