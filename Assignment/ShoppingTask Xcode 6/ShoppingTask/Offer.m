//
//  Offer.m
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "Offer.h"


@implementation Offer

-(int)valueOfDiscountFromList:(NSArray *)list{
    return 0;
    
}


//to be overridden in subclasses
-(BOOL)isApplicableToList:(NSArray *)list{
    return NO;
}

-(NSArray *)applicableProducts:(NSArray *)list{
    
    NSMutableArray *applicableProductsInList = [[NSMutableArray alloc]init];
    
    for (ShoppingItem *itemInShoppingList in list) {
        for (int i = 0; i <self.applicableProducts.count; i++) {
            int offerListId = [self.applicableProducts[i] intValue];
            
            if (offerListId == itemInShoppingList.productId) {
                [applicableProductsInList addObject:itemInShoppingList];
            }
        }
    }
    return applicableProductsInList;
    
}

// sort an array by price
-(NSArray *)sortListByPrice:(NSArray *)list {
    //store the list of products in offer in a mutable array
    NSMutableArray *productsInOffer = [[NSMutableArray alloc]initWithArray:list];
    //create a descriptor to sort the array with(on priceInPence)
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc]initWithKey:@"priceInPence" ascending:FALSE];
    //sort the array with the descriptor
    [productsInOffer sortUsingDescriptors:[[NSArray alloc]initWithObjects:descriptor, nil ]];
    return productsInOffer;
}



@end
