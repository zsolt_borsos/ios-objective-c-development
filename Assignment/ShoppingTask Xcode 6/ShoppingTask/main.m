//
//  main.m
//  ShoppingTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSLog(@"You need to run the tests, not the application, ⌘U will run tests!");
        
    }
    return 0;
}

