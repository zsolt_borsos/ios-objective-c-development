//
//  ShoppingItemsManager.m
//  ShoppingTask
//
//  Created by Andrew Muncey on 23/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "ShoppingItemsManager.h"
#import "ShoppingItem.h"

@implementation ShoppingItemsManager

+(int)priceForProductId:(int)prodId{
    NSBundle *classBundle = [NSBundle bundleForClass:[self class]];
    NSString *fileLocation = [classBundle pathForResource:@"Products" ofType:@"plist"];
    NSArray *productArray = [NSMutableArray arrayWithContentsOfFile:fileLocation];
    

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ProdId == %i",prodId];
    
    NSArray *filteredList = [productArray filteredArrayUsingPredicate:predicate];
    
    int price = 0;
    
    if (filteredList.count >0) {
        NSDictionary *item = [filteredList lastObject];
        price = [[item objectForKey:@"Price"]intValue];
    }
    
    return price;
}


@end
