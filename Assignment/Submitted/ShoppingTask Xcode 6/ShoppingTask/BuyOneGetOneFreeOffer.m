//
//  ByOneGetOneFreeOffer.m
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "BuyOneGetOneFreeOffer.h"

@implementation BuyOneGetOneFreeOffer

//this offer should apply to product ids 4 and 5


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.offerName = @"Buy One Get One Free on Coca-Cola";
        
        NSArray *products = @[@4,@5];
        self.applicableProducts = products;
    }
    return self;
}



-(BOOL)isApplicableToList:(NSArray *)list{
    
    //should return YES if a discount can be applied based on the supplied list of products (i.e. a product must be match an offer)
    
    //check all products in offer. If more than 1 then offer can be applied (return true)
    if ([self applicableProducts:list].count > 1) {
        return TRUE;
    }
    return FALSE;
    
}


-(int)valueOfDiscountFromList:(NSArray *)list{
    
    //order the list by price
    //discount the value of every second item in the list
    
    
    //store the list of products in offer in an array
    NSArray *productsInOffer = [[NSArray alloc] initWithArray:[self applicableProducts:list]];
    
    //sort the array
    productsInOffer = [self sortListByPrice:productsInOffer];
    
    int discount = 0;
    //counter of products
    int counter = 1;
    //check all products
    for (ShoppingItem *item in productsInOffer) {
        //if counter is even
        if (counter % 2 == 0) {
            //then add the price to discount
            discount += item.priceInPence;
        }
        counter++;
    }
 //return the discount amount
 return discount;
    
}


@end
