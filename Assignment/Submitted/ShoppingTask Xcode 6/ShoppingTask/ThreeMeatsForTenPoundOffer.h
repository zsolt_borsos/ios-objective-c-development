//
//  ThreeMeatsForTenPoundOffer.h
//  ShoppingTask
//
//  Created by Andrew Muncey on 23/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Offer.h"

@interface ThreeMeatsForTenPoundOffer : Offer

@end
