//
//  Offer.h
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShoppingItem.h"

@interface Offer : NSObject

//this is the offer name as shown on the reciept
@property (nonatomic,strong) NSString *offerName;

//this is a list of integers for each product that the offer applies to (e.g. barcodes)
@property (nonatomic,strong) NSArray *applicableProducts;

-(int)valueOfDiscountFromList:(NSArray *)list;

-(BOOL)isApplicableToList:(NSArray *)list;


/**
 Returns an array containing only the products to which the offer could be applied
 
 @param list
        The list of ShoppingItem objects to be checked
 @return The list of products to which the offer is applicable
 */
-(NSArray *)applicableProducts:(NSArray *)list;

//returns the sorted array (sorts by price)
-(NSArray *)sortListByPrice:(NSArray *)list;

@end
