//
//  HalfPriceOffer.m
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "HalfPriceOffer.h"

//removed this as it is included in main class (Offer)
//#import "ShoppingItem.h"

@implementation HalfPriceOffer

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.offerName = @"Half Price on Wine";

        NSArray *products = @[@8,@9];
        self.applicableProducts = products;
    }
    return self;
}


-(BOOL)isApplicableToList:(NSArray *)list{
    
    //should return YES if a discount can be applied based on the supplied list of products (i.e. a product must be match an offer)
    
    //if the returned list contains any elements then the offer is on, return true
    if ([self applicableProducts:list].count > 0) {
        return TRUE;
    }
    //otherwise return false
   return FALSE;
}


-(int)valueOfDiscountFromList:(NSArray *)list{
    
    //this method should iterate through the list and calculate the discounted price for each item
    
    //specifically it should create a running total of half of each applicable items price (as an integer)
    
    
    //declare a variable for discount price
    int discount = 0;
    
    //check each applicable item in list
    for (ShoppingItem *item in [self applicableProducts:list]) {
        
        /*
        //get half price
        int price = item.priceInPence / 2;
        
        //add the half price to discount on each iteration
        discount += price;
        */
        
        //the same as above, in a one liner
        discount += item.priceInPence / 2;
        
    }
    //return the sum of discount
    return discount;
    
 
}

@end
