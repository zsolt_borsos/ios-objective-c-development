//
//  FreeEvianWaterWithTimesNewsPaperOffer.m
//  ShoppingTask
//
//  Created by Andrew Muncey on 23/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "FreeEvianWaterWithTimesNewsPaperOffer.h"
#import "ShoppingItemsManager.h"

@implementation FreeEvianWaterWithTimesNewsPaperOffer{
    
    //track counters locally
    int _newspaperCounter;
    int _waterCounter;
    int _waterPrice;
    
}

//item 13 is times newspaper
//item 14 is evian mineral water

//for each newspaper bought the price of one mineral water (if purchased) should be discounted


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.offerName = @"Free Water when you but The Times";
        
        NSArray *products = @[@13,@14];
        self.applicableProducts = products;
        
        _newspaperCounter = 0;
        _waterCounter = 0;
        _waterPrice = 0;
        
    }
    return self;
}



-(BOOL)isApplicableToList:(NSArray *)list{
    
    //should return YES if a discount can be applied based on the supplied list of products (i.e. a product must be match an offer)

    
    //check all items in the list
    for (ShoppingItem *item in list) {
        
        //count items in list and store them
        if (item.productId == 13){
            _newspaperCounter++;
        }else if (item.productId == 14){
            _waterCounter++;
            //store water's price
            if (_waterPrice == 0){
                _waterPrice = item.priceInPence;
            }
        }
    }
    if (_newspaperCounter > 0) {
        if (_waterCounter > 0){
            //if there is at least 1 of each item then return true
            return TRUE;
        }
    }
    //if not already returned then return false
    return FALSE;
}




-(int)valueOfDiscountFromList:(NSArray *)list{
    
    //store the list of products in offer in an array
    NSArray *productsInOffer = [[NSArray alloc] initWithArray:[self applicableProducts:list]];
    
    //sort the array
    productsInOffer = [self sortListByPrice:productsInOffer];
    
    int discount = 0;
    
    int priceMultiplier = 0;
    //determine the multiplier for the price based on the number of items
    //only apply discount if there is at least 1 newspaper
    if (_newspaperCounter > 0) {
        //if there are more water than newspaper
        if (_newspaperCounter < _waterCounter) {
            //then use the number of newspapers as multiplier
            priceMultiplier = _newspaperCounter;
        }else {
            //otherwise use the number of waters as multiplier
            priceMultiplier = _waterCounter;
        }
    }
    //multiply the price of water by the multiplier
    discount = _waterPrice * priceMultiplier;
    //return the discount amount
    return discount;
    
}


@end
