//
//  Receipt.m
//  ShoppingTask
//
//  Created by Andrew Muncey on 23/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "ShoppingItem.h"
#import "Receipt.h"
#import "OfferApplicator.h"

@implementation Receipt{
    NSMutableArray *_discountedItems;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _discountedItems = [[NSMutableArray alloc]init];
    }
    return self;
}

-(int)priceBeforeDiscounts{
    
    //sum up the price of all items
  return 0;
}

-(int)discount{
    
    //get all the items that are OnOffer - hint you can check an objects class
    OfferApplicator *offerApplicator = [[OfferApplicator alloc]init];
    
    int discountTotal = 0;
    NSArray *discountLines = [offerApplicator offerDiscounts:self.items];
    
    for (RecieptOfferLine *line in discountLines) {
        discountTotal += line.valueInPence;
    }
    return  discountTotal;
}

-(int)finalPrice{
  return 0;
}

-(NSString *)print{
    
    NSMutableString *receiptPrintOut = [[NSMutableString alloc]init];
    
     [receiptPrintOut appendString:@"\n\n**********************************\n"];
    [receiptPrintOut appendString:@"***********Your Reciept***********\n\n"];
    
    //all products
    for (ShoppingItem *item in self.items) {
        
        
       [receiptPrintOut appendFormat:@"%@",item.name];
        //get item name length and apply dots to normalise price position
        for (int dots = 0; dots < 31-item.name.length; dots++) {
            [receiptPrintOut appendString:@"."];
        }
        [receiptPrintOut appendFormat:@"%i\n",item.priceInPence];
    }
    
    //sub total
    [receiptPrintOut appendFormat:@"\nSub Total:     %i\n",[self priceBeforeDiscounts]];
    
    //all discounts
    [receiptPrintOut appendFormat:@"\n*********Offers applied**********\n"];
    OfferApplicator *offerApp = [[OfferApplicator alloc]init];
    NSArray *discountLines = [offerApp offerDiscounts:self.items];
    for (RecieptOfferLine *line in discountLines) {
       [receiptPrintOut appendFormat:@"%@  -%i\n",line.name,line.valueInPence];
    }
    
    //discount total
    [receiptPrintOut appendFormat:@"\nYou have saved:     %i\n",[self discount]];

    //final total
    [receiptPrintOut appendFormat:@"\nGrand Total:        %i",[self finalPrice]];
    
         [receiptPrintOut appendString:@"\n*********************************\n\n"];
    
    NSLog(@"%@",receiptPrintOut);
    return receiptPrintOut;
}



@end
