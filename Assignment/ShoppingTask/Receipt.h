//
//  Receipt.h
//  ShoppingTask
//
//  Created by Andrew Muncey on 23/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Receipt : NSObject

@property (nonatomic,strong) NSMutableArray *items;

-(int)priceBeforeDiscounts;

-(int)discount;

-(int)finalPrice;


-(NSString *)print;

@end
