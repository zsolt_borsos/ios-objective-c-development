//
//  ShoppingTask_Tests.m
//  ShoppingTask Tests
//
//  Created by Andrew Muncey on 23/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ShoppingItem.h"
#import "Receipt.h"

//uncomment to test detergent / softener offer class (also uncomment final 5 tests in this class)
//#import "FreeSoftenerWithDetergentOffer.h"


@interface ShoppingTask_Tests : XCTestCase

@end

@implementation ShoppingTask_Tests{
    NSMutableArray *_allProducts;
}

- (void)setUp
{
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    //get the bundle for the tests, locate the list of products and load them into an array
    NSBundle *classBundle = [NSBundle bundleForClass:[self class]];
    NSString *fileLocation = [classBundle pathForResource:@"Products" ofType:@"plist"];
    NSArray *productArray = [NSMutableArray arrayWithContentsOfFile:fileLocation];
    
    //instantiate instance variable to hold products
    _allProducts = [[NSMutableArray alloc]init];
    
    //iterate through the list from file and create shopping list items for each one
    for (NSDictionary *dict in productArray) {
        ShoppingItem *item = [[ShoppingItem alloc]init];
        item.name = [dict objectForKey:@"Name"];
        item.priceInPence = [[dict  objectForKey:@"Price"]intValue];
        item.productId = [[dict objectForKey:@"ProdId"]intValue];
        [_allProducts addObject:item];
    }
    
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testSumAllProducts{
    NSMutableArray *products = [[NSMutableArray alloc]init];

    //two products from the list
    [products addObject:_allProducts[7]];
    [products addObject:_allProducts[8]];
    
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    XCTAssertEqual([receipt priceBeforeDiscounts], 798, @"Prices should match");
}

-(void)testHalfPrice{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[7]];
    [products addObject:_allProducts[8]];
    
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    //note that the price is halved for each item (as an integer), so may not equal sum of products / 2
    XCTAssertEqual([receipt discount],398,@"Half price not calculated correctly");
}

-(void)testHalfPriceWithSomeIneligibleItems{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[7]];
    [products addObject:_allProducts[9]];
    [products addObject:_allProducts[12]];
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    //note that the price is halved for each item (as an integer), so may not equal sum of products / 2
    XCTAssertEqual([receipt discount],249,@"Half price not calculated correctly");
}


-(void)testBOGOFEvenNumberOfPoducts{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[3]];
    [products addObject:_allProducts[4]];
    [products addObject:_allProducts[4]];
    [products addObject:_allProducts[4]];
    
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    //note that the price is halved for each item (as an integer), so may not equal sum of products / 2
    XCTAssertEqual([receipt discount],400,@"Half price not calculated correctly");
}

-(void)testBOGOFOddNumberOfPoducts{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[3]];
    [products addObject:_allProducts[4]];
    [products addObject:_allProducts[4]];
  
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    //note that the price is halved for each item (as an integer), so may not equal sum of products / 2
    XCTAssertEqual([receipt discount],200,@"Half price not calculated correctly");
}


-(void)testThreeMeatForTenPoundsThreeItems{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[5]];
    [products addObject:_allProducts[6]];
    [products addObject:_allProducts[9]];
    
    
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    //note that the price is halved for each item (as an integer), so may not equal sum of products / 2
    XCTAssertEqual([receipt discount],250,@"Meat discount not calculated correctly");
}

-(void)testThreeMeatForTenPoundsFiveItems{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[5]];
    [products addObject:_allProducts[6]];
    [products addObject:_allProducts[9]];
    [products addObject:_allProducts[10]];
    [products addObject:_allProducts[11]];
    
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    //note that the price is halved for each item (as an integer), so may not equal sum of products / 2
    XCTAssertEqual([receipt discount],350,@"Meat discount not calculated correctly");
}


-(void)testThreeFreeWatersWithTwoPapers{
    //only two of the three waters should be free
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[12]];
    [products addObject:_allProducts[13]];
    [products addObject:_allProducts[12]];
    [products addObject:_allProducts[13]];
    [products addObject:_allProducts[13]];
    
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    //note that the price is halved for each item (as an integer), so may not equal sum of products / 2
    XCTAssertEqual([receipt discount],200,@"Water free with paper discount not calculated correctly");
}

-(void)testTwoFreeWatersWithThreePaper{
    //only two discounts should be applied
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[12]];
    [products addObject:_allProducts[13]];
    [products addObject:_allProducts[12]];
    [products addObject:_allProducts[12]];
    [products addObject:_allProducts[13]];
    
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    //note that the price is halved for each item (as an integer), so may not equal sum of products / 2
    XCTAssertEqual([receipt discount],200,@"Water free with paper discount not calculated correctly");
}


-(void)testBuyTwoGetThirdFree6ItemsCheapestFree{
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[14]];
    [products addObject:_allProducts[14]];
    [products addObject:_allProducts[15]];
    [products addObject:_allProducts[15]];
    [products addObject:_allProducts[15]];
    [products addObject:_allProducts[15]];
    
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    XCTAssertEqual([receipt discount],438,@"Buy two get third free not correct with 5 items");
}

-(void)testBuyTwoGetThirdFree6ItemsOneOfEachPriceFree{
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[14]];
    [products addObject:_allProducts[14]];
    [products addObject:_allProducts[14]];
    [products addObject:_allProducts[15]];
    [products addObject:_allProducts[15]];
    [products addObject:_allProducts[15]];
    
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    XCTAssertEqual([receipt discount],538,@"Buy two get third free not correct with 5 items");
}

-(void)testBuyTwoGetThirdFree5Items{
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[14]];
    [products addObject:_allProducts[14]];
    [products addObject:_allProducts[15]];
    [products addObject:_allProducts[15]];
    [products addObject:_allProducts[15]];
    
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    XCTAssertEqual([receipt discount],219,@"Buy two get third free not correct with 5 items");
}


-(void)testNoOffersWithIneligibleProducts{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[1]];
    [products addObject:_allProducts[2]];
    [products addObject:_allProducts[0]];

    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    XCTAssertEqual([receipt discount],0,@"Discount incorrectly applies, no items qualify");
    
}


-(void)testNoOffersWithEligibleProducts{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[1]];
    [products addObject:_allProducts[2]];
    [products addObject:_allProducts[3]];
    [products addObject:_allProducts[0]];
    [products addObject:_allProducts[5]];
    [products addObject:_allProducts[6]];
    [products addObject:_allProducts[13]];
    [products addObject:_allProducts[14]];

    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    XCTAssertEqual([receipt discount],0,@"Discount incorrectly applies, no items qualify");
    
}

-(void)testRecieptExcludeSoftener{
    //load a sample text file containing the receipt that should be generated
    NSBundle *classBundle = [NSBundle bundleForClass:[self class]];
    NSString *fileLocation = [classBundle pathForResource:@"TestReceipt" ofType:@"txt"];
    NSString *testReceiptText = [NSString stringWithContentsOfFile:fileLocation encoding:NSUTF8StringEncoding error:nil];
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[7]];
    [products addObject:_allProducts[8]];
    [products addObject:_allProducts[3]];
    [products addObject:_allProducts[4]];
    [products addObject:_allProducts[4]];
    [products addObject:_allProducts[4]];
    [products addObject:_allProducts[5]];
    [products addObject:_allProducts[6]];
    [products addObject:_allProducts[9]];
    [products addObject:_allProducts[10]];
    [products addObject:_allProducts[11]];
    [products addObject:_allProducts[12]];
    [products addObject:_allProducts[13]];
    [products addObject:_allProducts[12]];
    [products addObject:_allProducts[13]];
    [products addObject:_allProducts[13]];
    [products addObject:_allProducts[14]];
    [products addObject:_allProducts[14]];
    [products addObject:_allProducts[14]];
    [products addObject:_allProducts[15]];
    [products addObject:_allProducts[15]];
    [products addObject:_allProducts[15]];
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
   NSString *receiptText = [receipt print];
    BOOL equalStrings = [receiptText isEqualToString:testReceiptText];
    XCTAssertTrue(equalStrings, @"Test Reciept and Actual Reciept are not the same");
}
/*
-(void)testSoftenerInitMethod{

    FreeSoftenerWithDetergentOffer *softOffer = [[FreeSoftenerWithDetergentOffer alloc]init];
    
    int prodIdSum = 0;
    
    for (int i=0; i<softOffer.applicableProducts.count; i++) {
        prodIdSum += [softOffer.applicableProducts[i] intValue];
    }
    
    XCTAssertTrue(prodIdSum == 74, @"Products not correctly initialised in Softner offer");
}
-(void)testSoftenerApplicableItemsNotApplicable{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[16]];
    [products addObject:_allProducts[17]];
 
    
    FreeSoftenerWithDetergentOffer *softOffer = [[FreeSoftenerWithDetergentOffer alloc]init];
    
    XCTAssertFalse([softOffer isApplicableToList:products],@"Incorrectly identifies list as valid");
}

-(void)testSoftenerApplicableItemsApplicable{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[16]];
    [products addObject:_allProducts[18]];
    [products addObject:_allProducts[17]];
    [products addObject:_allProducts[19]];
    
    [products addObject:_allProducts[2]];

    FreeSoftenerWithDetergentOffer *softOffer = [[FreeSoftenerWithDetergentOffer alloc]init];
    
    XCTAssertTrue([softOffer isApplicableToList:products],@"Incorrectly identifies valid list as invalid");
}

-(void)testSoftenerDetergent{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[16]];
    [products addObject:_allProducts[17]];
    [products addObject:_allProducts[19]];
    [products addObject:_allProducts[18]];
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    XCTAssertEqual([receipt discount], 828, @"Discount for softener not correct");
}

-(void)testSoftenerDetergentOneQualifyingProduct{
    
    NSMutableArray *products = [[NSMutableArray alloc]init];
    [products addObject:_allProducts[17]];
    [products addObject:_allProducts[19]];
    [products addObject:_allProducts[18]];
    Receipt *receipt = [[Receipt alloc]init];
    
    receipt.items = products;
    
    XCTAssertEqual([receipt discount], 429, @"Discount for softener not correct");
}
*/

@end
