//
//  RecieptOfferLine.h
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecieptOfferLine : NSObject

//this class simply contains the name of the offer and the value of it

@property (nonatomic,strong) NSString *name;
@property (assign) int valueInPence;


@end
