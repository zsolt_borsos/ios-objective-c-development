//
//  BuyTwoGetThirdFreeOffer.m
//  ShoppingTask
//
//  Created by Andrew Muncey on 23/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "BuyTwoGetThirdFreeOffer.h"

@implementation BuyTwoGetThirdFreeOffer

//this offer should order applicable products by price and add a discount to the value of every third item

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.offerName = @"Buy Two Get Third Free on Deodorants";
        
        NSArray *products = @[@15,@16];
        self.applicableProducts = products;
    }
    return self;
}



-(BOOL)isApplicableToList:(NSArray *)list{
    
    //should return YES if a discount can be applied based on the supplied list of products (i.e. a product must be match an offer)
    return 0;
}


-(int)valueOfDiscountFromList:(NSArray *)list{
    
    //order the list by price
    
    //discount the value of every second item in the list
    
return 0;
    
}




@end
