//
//  ThreeMeatsForTenPoundOffer.m
//  ShoppingTask
//
//  Created by Andrew Muncey on 23/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "ThreeMeatsForTenPoundOffer.h"

@implementation ThreeMeatsForTenPoundOffer

//this offer should apply to product ids 6,7,10,11 and 12

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.offerName = @"Three meats for £10";
        
        NSArray *products = @[@6,@7,@10,@11,@12];
        self.applicableProducts = products;
    }
    return self;
}



-(BOOL)isApplicableToList:(NSArray *)list{
    
    //should return YES if a discount can be applied based on the supplied list of products (i.e. a product must be match an offer)
  return 0;
}


-(int)valueOfDiscountFromList:(NSArray *)list{
    
    //order the list by price (i.e. maximise customer savings)
    
    //for each of groups of three meat items, add a discount value that caps the price at 1000 pence
    
    return 0;
    
}





@end
