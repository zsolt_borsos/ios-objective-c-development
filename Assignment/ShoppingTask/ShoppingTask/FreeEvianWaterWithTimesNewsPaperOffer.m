//
//  FreeEvianWaterWithTimesNewsPaperOffer.m
//  ShoppingTask
//
//  Created by Andrew Muncey on 23/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "FreeEvianWaterWithTimesNewsPaperOffer.h"
#import "ShoppingItemsManager.h"

@implementation FreeEvianWaterWithTimesNewsPaperOffer

//item 13 is times newspaper
//item 14 is evian mineral water

//for each newspaper bought the price of one mineral water (if purchased) should be discounted


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.offerName = @"Free Water when you but The Times";
        
        NSArray *products = @[@13,@14];
        self.applicableProducts = products;
    }
    return self;
}



-(BOOL)isApplicableToList:(NSArray *)list{
    
    //should return YES if a discount can be applied based on the supplied list of products (i.e. a product must be match an offer)

    return 0;
}




-(int)valueOfDiscountFromList:(NSArray *)list{
    
    return 0;
    
}


@end
