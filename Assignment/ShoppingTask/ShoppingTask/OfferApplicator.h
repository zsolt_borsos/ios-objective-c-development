//
//  OfferApplicator.h
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecieptOfferLine.h"

@interface OfferApplicator : NSObject


-(NSArray*)offerDiscounts:(NSArray *)list;

@end
