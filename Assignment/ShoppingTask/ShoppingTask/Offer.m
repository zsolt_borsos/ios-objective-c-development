//
//  Offer.m
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "Offer.h"


@implementation Offer

-(int)valueOfDiscountFromList:(NSArray *)list{
    return 0;
    
    //code to order list taken and adapted form
    //http://stackoverflow.com/questions/3402667/sort-an-nsarray-in-descending-order
    
    
    //code here
    
    
    
    //end of code taken form web
    
    
}


//to be overridden in subclasses
-(BOOL)isApplicableToList:(NSArray *)list{
    return NO;
}

-(NSArray *)applicableProducts:(NSArray *)list{
    
    NSMutableArray *applicableProductsInList = [[NSMutableArray alloc]init];
    
    for (ShoppingItem *itemInShoppingList in list) {
        for (int i = 0; i <self.applicableProducts.count; i++) {
            int offerListId = [self.applicableProducts[i] intValue];
            
            if (offerListId == itemInShoppingList.productId) {
                [applicableProductsInList addObject:itemInShoppingList];
            }
        }
    }
    return applicableProductsInList;
    
}

@end
