//
//  ByOneGetOneFreeOffer.h
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "Offer.h"

@interface BuyOneGetOneFreeOffer : Offer

@end
