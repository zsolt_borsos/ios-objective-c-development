//
//  ByOneGetOneFreeOffer.m
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "BuyOneGetOneFreeOffer.h"

@implementation BuyOneGetOneFreeOffer

//this offer should apply to product ids 4 and 5


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.offerName = @"Buy One Get One Free on Coca-Cola";
        
        NSArray *products = @[@4,@5];
        self.applicableProducts = products;
    }
    return self;
}



-(BOOL)isApplicableToList:(NSArray *)list{
    
    //should return YES if a discount can be applied based on the supplied list of products (i.e. a product must be match an offer)
  return 0;
}


-(int)valueOfDiscountFromList:(NSArray *)list{
    
    //order the list by price
    
    //discount the value of every second item in the list
    
 return 0;
    
}


@end
