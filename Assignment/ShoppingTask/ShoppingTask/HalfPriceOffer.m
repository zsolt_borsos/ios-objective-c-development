//
//  HalfPriceOffer.m
//  ShoppingAssignmentTask
//
//  Created by Andrew Muncey on 22/09/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "HalfPriceOffer.h"
#import "ShoppingItem.h"

@implementation HalfPriceOffer

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.offerName = @"Half Price on Wine";

        NSArray *products = @[@8,@9];
        self.applicableProducts = products;
    }
    return self;
}


-(BOOL)isApplicableToList:(NSArray *)list{
    
    //should return YES if a discount can be applied based on the supplied list of products (i.e. a product must be match an offer)
   return 0;
}


-(int)valueOfDiscountFromList:(NSArray *)list{
    
    //this method should iterate through the list and calculate the discounted price for each item
    
    //specifically it should create a running total of half of each applicable items price (as an integer)
    
return 0;
 
}

@end
