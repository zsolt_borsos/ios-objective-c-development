
// This class is complete. Do not modify it

#import <Foundation/Foundation.h>
#import "Offer.h"

@interface ShoppingItem : NSObject

@property (nonatomic,strong) NSString *name;
@property (assign) int priceInPence;
@property (assign)int productId;



@end
