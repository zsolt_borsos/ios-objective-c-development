//
//  GreetingManager.h
//  GreetingsApp
//
//  Created by student on 13/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GreetingManager : NSObject

@property (nonatomic, strong) NSString *name;

-(void)storeExclamation:(NSString *)theExclamation;
-(NSString *)composeGreeting;
-(NSString *)allGreetings;



@end
