//
//  main.m
//  GreetingsApp
//
//  Created by student on 13/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GreetingManager.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
    
        GreetingManager *greetingManager = [[GreetingManager alloc] init];
        
        [greetingManager storeExclamation:@"Hello"];
        greetingManager.name = @"Zsolt";
        
        NSString *composedGreeting = [greetingManager composeGreeting];
        
        NSLog(@"%@", composedGreeting);
        
        //add twice to test duplicate greetings are not added
        [greetingManager storeExclamation:@"Bonjour"];
        [greetingManager storeExclamation:@"Bonjour"];
        
        [greetingManager storeExclamation:@"Good morning"];
        
        //[greetingManager removeGreeting:@"Hello"];
        
        //log all greetings
        NSLog(@"%@",[greetingManager allGreetings]);

        composedGreeting = [greetingManager composeGreeting];
        NSLog(@"%@", composedGreeting);

        composedGreeting = [greetingManager composeGreeting];
        NSLog(@"%@", composedGreeting);
        
        composedGreeting = [greetingManager composeGreeting];
        NSLog(@"%@", composedGreeting);
        composedGreeting = [greetingManager composeGreeting];
        NSLog(@"%@", composedGreeting);

    }
    return 0;
}

