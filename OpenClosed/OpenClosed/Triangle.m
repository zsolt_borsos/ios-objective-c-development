//
//  Triangle.m
//  OpenClosed
//
//  Created by Andrew Muncey on 01/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "Triangle.h"

@implementation Triangle

-(double)calculateArea {
    double area = 0;
    area = (self.height * self.width) / 2;
    return area;
}


@end
