//
//  Triangle.h
//  OpenClosed
//
//  Created by Andrew Muncey on 01/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "Shape.h"

@interface Triangle : NSObject <ShapeProtocol>

@property (assign) double width;
@property (assign) double height;

@end
