//
//  Shape.h
//  OpenClosed
//
//  Created by Andrew Muncey on 01/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shape.h"

@interface Rectangle : NSObject <ShapeProtocol>

@property (assign) double width;
@property (assign) double height;

@end
