//
//  Circle.h
//  OpenClosed
//
//  Created by student on 08/12/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shape.h"

@interface Circle : NSObject <ShapeProtocol>

@property (assign) double radius;

@end
