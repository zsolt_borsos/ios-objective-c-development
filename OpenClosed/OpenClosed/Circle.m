//
//  Circle.m
//  OpenClosed
//
//  Created by student on 08/12/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "Circle.h"

@implementation Circle

-(double)calculateArea {
    double area = 0;
    area = M_PI * self.radius * self.radius;
    return area;
}

@end
