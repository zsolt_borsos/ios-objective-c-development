//
//  Rectangle.m
//  OpenClosed
//
//  Created by Andrew Muncey on 01/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "Rectangle.h"

@implementation Rectangle

-(double)calculateArea {
    double area = 0;
    area = self.width * self.height;
    return area;
}

@end
