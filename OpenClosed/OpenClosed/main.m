//
//  main.m
//  OpenClosed
//
//  Created by Andrew Muncey on 01/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Rectangle.h"
#import "Triangle.h"
#import "Circle.h"


int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        
        //rectangle
        Rectangle *rect = [[Rectangle alloc]init];
        rect.width = 4.0;
        rect.height = 8.0;
        //double area = [AreaCalculator calculateArea:rect];
        double area = [rect calculateArea];
        NSLog(@"Area is: %f",area);
        
        
        //triangle
        Triangle *tri = [[Triangle alloc]init];
        tri.width = 4.0;
        tri.height = 3.0;
        area = [tri calculateArea];
        NSLog(@"Area is: %f",area);
        
        
        //circle
        Circle *cir = [[Circle alloc]init];
        cir.radius = 5.0;
        area = [cir calculateArea];
        NSLog(@"Area is: %f",area);
        
    }
    return 0;
}

