//
//  main.m
//  BingoMachine
//
//  Created by student on 20/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BingoMachine.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        //create a new bingo object
        
        //create with balls
        //BingoMachine *myBingo = [[BingoMachine alloc] initWithBalls:40];
        
        //create default bingo (with 90 balls)
        BingoMachine *myBingo = [[BingoMachine alloc] init];
        
        //shuffle the tumbler
        [myBingo shuffleTumbler];
        
        NSInteger number;
        // play the game
        // in for loop:
        // 1. initialise: get a new number and assing it to number.
        // 2. condition: while number is not 0
        // 3. iteration: get the next number and assign it to number
        for (number = [myBingo getNextNumber]; number != 0 ; number = [myBingo getNextNumber]) {
            
            //print out the next number
            NSLog(@"The next number is: %li", (long)number);
            //numbers left
            NSLog(@"Numbers left: %li", (long)[myBingo getRemainingBallsCount]);
            
            //wait for a sec
            //sleep(1);
        }
        
        //check number, expecting 0  --debug
        //NSLog(@"number: %li", (long)number);
        
        //check arrays
        [myBingo getDebugInfo];
        
        
        
    }
    return 0;
}

