//
//  BingoMachine.m
//  BingoMachine
//
//  Created by student on 20/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import "BingoMachine.h"

@implementation BingoMachine
{
    NSMutableArray *_tumblerArray;
    NSMutableArray *_usedNumbers;
    NSInteger _ballsInGame;
    
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        _usedNumbers = [[NSMutableArray alloc]init];
        _tumblerArray = [[NSMutableArray alloc]init];
        _ballsInGame = 90;
        for (int i = 1; i <= _ballsInGame; i++){
            NSNumber *ball = [NSNumber numberWithInt:i];
            [_tumblerArray addObject:ball];
        }
        
    }
    return self;
}

-(id)initWithBalls:(NSInteger)balls {
    
    self = [super init];
    if (self) {
        _usedNumbers = [[NSMutableArray alloc]init];
        _tumblerArray = [[NSMutableArray alloc]init];
        _ballsInGame = balls;
        for (int i = 1; i <= _ballsInGame; i++){
            NSNumber *ball = [NSNumber numberWithInt:i];
            [_tumblerArray addObject:ball];
        }
        
    }
    return self;
    
}

-(void)shuffleTumbler{
    
    //set higher limit for better shuffling
    int shuffleLimit = 50;
    //shuffle array
    for (int i = 0; i < shuffleLimit; i++) {
        //get a random position
        int changeFrom = arc4random_uniform((int)_ballsInGame);
        int changeTo;
        //keep the "from" position for 5 swaps
        for (int k = 0; k < 5; k++) {
            //get another random number to change position with
            for (changeTo = arc4random_uniform((int)_ballsInGame); changeTo == changeFrom; changeTo = arc4random_uniform((int)_ballsInGame)) {
                //do nothing. "Calculation" is in the loop.
            }
            
            //NSLog(@"Change to: %i Change from: %i", changeTo, changeFrom);
            //swap positions in array using the 2 random numbers
            [_tumblerArray exchangeObjectAtIndex:changeFrom withObjectAtIndex:changeTo];
        }
    }
    //NSLog(@"Array: %@", [_tumblerArray description]);
    
}

-(NSInteger)getNextNumber{
    
    //create a number object and store the last element of the array in
    NSNumber *nextNumber = [[NSNumber alloc]init];
    nextNumber = [_tumblerArray lastObject];
    
    //remove the last element of the tumbler array and add it to the used numbers array
    [_tumblerArray removeObject:nextNumber];
    if (nextNumber){
        [_usedNumbers addObject:nextNumber];
    }
    
    //return the result, the integer value
    NSInteger result = [nextNumber integerValue];
    return result;
}


-(BOOL)checkNumber:(int)number{

    NSNumber *ball = [[NSNumber numberWithInt:number]init];
    if ([_usedNumbers containsObject:ball]) {
        return TRUE;
    }else{
        return FALSE;
    }
    
}

-(NSInteger)getRemainingBallsCount{
    NSInteger remaining = _tumblerArray.count;
    return remaining;
}


-(void)getDebugInfo{
    
    NSLog(@"tumblerArray: %lu", (unsigned long)_tumblerArray.count);
    NSLog(@"Used numbers: %lu", (unsigned long)_usedNumbers.count);
    
    
}



@end
