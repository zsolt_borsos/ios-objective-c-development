//
//  BingoMachine.h
//  BingoMachine
//
//  Created by student on 20/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BingoMachine : NSObject

-(id)initWithBalls:(NSInteger)balls;
-(void)shuffleTumbler;
-(NSInteger)getNextNumber;
-(BOOL)checkNumber:(int)number;
-(NSInteger)getRemainingBallsCount;
-(void)getDebugInfo;

@end
