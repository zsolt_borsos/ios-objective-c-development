//
//  Lock.h
//  DependencyInversion
//
//  Created by Zsolt Borsos on 12/01/2015.
//  Copyright (c) 2015 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Lock : NSObject

-(void)lock;
-(void)unlock;

@property (nonatomic) BOOL locked;

@end
