//
//  Lock.m
//  DependencyInversion
//
//  Created by Zsolt Borsos on 12/01/2015.
//  Copyright (c) 2015 University of Chester. All rights reserved.
//

#import "Lock.h"

@implementation Lock

-(void)lock{
    self.locked = YES;
}
-(void)unlock{
    self.locked = NO;
}

@end
