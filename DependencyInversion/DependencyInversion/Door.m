//
//  Door.m
//  DependencyInversion
//
//  Created by Andrew Muncey on 02/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import "Door.h"

@implementation Door


-(BOOL)openDoor{
    if (self.lock.locked) {
        return NO;
    }
    else{
        return YES;
    }
}

@end
