//
//  main.m
//  DependencyInversion
//
//  Created by Andrew Muncey on 02/04/2014.
//  Copyright (c) 2014 University of Chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MorticeLock.h"
#import "Door.h"


int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
       
        
        Door *frontDoor = [[Door alloc]init];
        MorticeLock *chubbLock = [[MorticeLock alloc]init];
        
        frontDoor.lock = chubbLock;
        
        [frontDoor.lock lock];
        
        if ([frontDoor openDoor]) {
            NSLog(@"Door opened");
        } else{
            NSLog(@"Door cannot be opened");
        }
        
        
    }
    return 0;
}

