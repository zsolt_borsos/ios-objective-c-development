//
//  main.m
//  ArrayDemo
//
//  Created by student on 13/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSURL *myUrl = [NSURL URLWithString:@"http://amuncey.studentwebserver.co.uk/FirstArray.txt"];
        
        NSArray *myArray = [NSArray arrayWithContentsOfURL: myUrl];
        
        long itemsInArrayCount = [myArray count];
        
        NSLog(@"There are %li items", itemsInArrayCount);
        
        id item = [myArray lastObject];
        
        NSLog(@"%@", [item class]);
        
        long elephantPosition = [myArray indexOfObject:@"Elephant"];
        
        NSLog(@"Elephant is at %li", elephantPosition);
        
        
        
        
        NSMutableArray *mutableArray = [myArray mutableCopy];
        NSString *lastItem = [mutableArray lastObject];
        NSLog(@"Last item is a %@", lastItem);
        
        for (int i = 0; i < 3; i++){
            NSLog(@"%@", [myArray objectAtIndex:i]);
        }
        
        for (NSString *animal in mutableArray) {
            NSLog(@"%@", animal);
        }
        
        //long lionPosition
        
    }
    return 0;
}

