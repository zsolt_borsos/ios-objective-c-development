//
//  scoreTests.m
//  TennisScore
//
//  Created by Zsolt Borsos on 19/01/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Game.h"
#import "Player.h"

@interface scoreTests : XCTestCase

@end

@implementation scoreTests{
    Game *_game;
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _game = [[Game alloc]init];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _game = NULL;
}

-(void)testGameCreated {
    
    XCTAssertNotNil(_game, @"The game object is null.");
}

-(void)testPlayer1Exist {
    
    XCTAssertNotNil(_game.p1, @"Player 1 object is null");

}
-(void)testPlayer2Exist {
    
    XCTAssertNotNil(_game.p2, @"Player 2 object is null");
}

-(void)testP1IsPlayerClass {
    
    Player *p = [[Player alloc]init];
    
    XCTAssertNotEqualObjects(p, _game.p1, @"Player 1 is not made of Player!");
}

-(void)testP2IsPlayerClass {
    
    Player *p = [[Player alloc]init];
    
    XCTAssertNotEqualObjects(p, _game.p2, @"Player 2 is not made of Player!");
}

-(void)testPlayerStartsWithZeroScore {
    
    Player *p = [[Player alloc]init];
    XCTAssertEqual(p.score, 0, @"Player's score is not 0 at start!");
}

-(void)testPlayerCanScore {
    

    
    
}


@end
