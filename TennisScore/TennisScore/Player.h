//
//  Player.h
//  TennisScore
//
//  Created by Zsolt Borsos on 19/01/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Player : NSObject

@property int score;

@end
