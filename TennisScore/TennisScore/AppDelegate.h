//
//  AppDelegate.h
//  TennisScore
//
//  Created by Zsolt Borsos on 19/01/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
