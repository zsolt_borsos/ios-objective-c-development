//
//  Game.h
//  TennisScore
//
//  Created by Zsolt Borsos on 19/01/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface Game : NSObject

    @property Player *p1;
    @property Player *p2;

@end
