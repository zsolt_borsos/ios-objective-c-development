//
//  Player.m
//  TennisScore
//
//  Created by Zsolt Borsos on 19/01/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Player.h"

@implementation Player

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.score = 0;
    }
    return self;
}

@end
