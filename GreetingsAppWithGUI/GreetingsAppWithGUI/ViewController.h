//
//  ViewController.h
//  GreetingsAppWithGUI
//
//  Created by Zsolt Borsos on 21/11/2014.
//  Copyright (c) 2014 ___ZsoltBorsos___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *greetingsLabel;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *exclamation;
- (IBAction)setUserNameButton:(UIButton *)sender;
- (IBAction)getNewGreetings:(UIButton *)sender;
- (IBAction)addNewExclamation:(UIButton *)sender;

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
- (IBAction) clickedBackground;

@end
