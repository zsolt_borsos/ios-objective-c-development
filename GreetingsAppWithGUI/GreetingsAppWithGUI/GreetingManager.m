//
//  GreetingManager.m
//  GreetingsApp
//
//  Created by student on 13/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import "GreetingManager.h"

@implementation GreetingManager{
    NSString *_exclamation;
    NSMutableArray *_exclamationList;
}


- (id)init
{
    self = [super init];
    if (self)
    {        
        //initialise the array
        _exclamationList = [[NSMutableArray alloc]init];
        [_exclamationList addObject:@"Greetings"];
    }
    return self;
}


-(void)storeExclamation:(NSString *)theExclamation{
    
    //_exclamation = theExclamation;
    if (![_exclamationList containsObject:theExclamation]) {
        [_exclamationList addObject:theExclamation];
    }
    
}

-(NSString *)composeGreeting{
    //get random greeting
    int itemsInArray = _exclamationList.count;
    int randomPos = arc4random_uniform(itemsInArray);
    _exclamation = _exclamationList[randomPos];
    
    NSString *greeting = [NSString stringWithFormat:@"%@ %@", _exclamation, self.name];
    return greeting;
}

-(NSString *)allGreetings {
    NSMutableString *exclamations = [[NSMutableString alloc]init];
    
    //check exclamations
    for (int i = 0; i < _exclamationList.count; i++) {
        [exclamations appendString: _exclamationList[i]];
        //separator if not at the end
        if (i != (_exclamationList.count - 1)) {
            [exclamations appendString:@", "];
        }
    }
    return exclamations;
    
    /* from tutorial
    for (NSString *exclamation in _exclamationList){
        
        [exclamations appendString:exclamation];
        //separator
        [exclamations appendString:@", "];
    }
    */

}


@end
