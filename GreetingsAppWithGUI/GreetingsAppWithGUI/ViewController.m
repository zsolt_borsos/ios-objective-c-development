//
//  ViewController.m
//  GreetingsAppWithGUI
//
//  Created by Zsolt Borsos on 21/11/2014.
//  Copyright (c) 2014 ___ZsoltBorsos___. All rights reserved.
//

#import "ViewController.h"
#import "GreetingManager.h"

@interface ViewController ()

@end

@implementation ViewController{
    GreetingManager *_app;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _app = [[GreetingManager alloc]init];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction) clickedBackground
{
    [self.view endEditing:YES];
}


- (IBAction)setUserNameButton:(UIButton *)sender {
    _app.name = _userName.text;
    _greetingsLabel.text = _app.composeGreeting;
    
}
- (IBAction)getNewGreetings:(UIButton *)sender {
    _greetingsLabel.text = _app.composeGreeting;
    
}

- (IBAction)addNewExclamation:(UIButton *)sender {
    [_app storeExclamation:_exclamation.text];
}

@end
