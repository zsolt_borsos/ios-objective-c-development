//
//  Questions.h
//  QuestionBank
//
//  Created by Zsolt Borsos on 17/11/2014.
//  Copyright (c) 2014 Andrew Muncey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Questions : NSObject

-(NSDictionary *)getNextQuestion;
-(void)saveQuestion:(NSString *)question withAnswer:(NSString *)answer;
-(BOOL)isSavedDataExist;

@end
