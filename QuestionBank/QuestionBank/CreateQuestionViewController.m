//
//  SecondViewController.m
//  QuestionBank
//
//  Created by Andrew Muncey on 03/02/2013.
//  Copyright (c) 2013 Andrew Muncey. All rights reserved.
//

#import "CreateQuestionViewController.h"
#import "QuestionsAdvanced.h"
//#import "Questions.h"

@interface CreateQuestionViewController ()

@end

@implementation CreateQuestionViewController{
    //with questions.h
    //Questions *_questions;
    QuestionsAdvanced *_questions;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //_questions = [[Questions alloc]init];
    _questions = [[QuestionsAdvanced alloc]init];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)addQuestion:(id)sender {
    
    //the commented-out below code takes the values from the textfields view and stores them each in an NSString
    //you should write code to add the question (as a dictionary with two key value pairs) to an array
    //TASKS
    //load the array from disk
    //add the question to the array
    //save the to disk
    
    NSString *question = self.createQuestionText.text;
    NSString *answer = self.createAnswerText.text;
    
    //with questions.h
    //[_questions saveQuestion:question withAnswer:answer];
    
    //new way
    [_questions saveQuestion:question withAnswer:answer];
    
    
    //clear the text boxes
    self.createAnswerText.text = @"";
    self.createQuestionText.text = @"";
    
}

- (IBAction)touchedBackground:(id)sender {
      [self.view endEditing:YES];
}
@end
