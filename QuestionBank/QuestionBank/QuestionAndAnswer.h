//
//  QuestionAndAnswer.h
//  QuestionBank
//
//  Created by Zsolt Borsos on 23/11/2014.
//  Copyright (c) 2014 Andrew Muncey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionAndAnswer : NSObject

@property (nonatomic, weak) NSString *question;
@property (nonatomic, weak) NSString *answer;

@end
