//
//  QuestionsAdvanced.m
//  QuestionBank
//
//  Created by Zsolt Borsos on 23/11/2014.
//  Copyright (c) 2014 Andrew Muncey. All rights reserved.
//

#import "QuestionsAdvanced.h"

@implementation QuestionsAdvanced {
    NSString *_docPath;
}

-(instancetype)init {
    self = [super init];
    if (self){
        [self setDocPath];
        _questions = [[NSMutableArray alloc]init];
        [self loadDataFromFile];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        [self setDocPath];
        //_questions = [[NSMutableArray alloc]init];
        self.questions = [coder decodeObjectForKey:@"questions"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.questions forKey:@"questions"];
    
}


-(QuestionAndAnswer *)getNextQuestion{
    if ([self loadDataFromFile]){
        /*
        if (_questions.count > 0) {
            int randomPos = arc4random_uniform(_questions.count);
            QuestionAndAnswer *question = [[QuestionAndAnswer alloc]init];
            question = _questions[randomPos];
            return question;
        }
         */
        
    }
    return nil;
}


-(void)saveQuestion:(NSString *)question withAnswer:(NSString *)answer{
    QuestionAndAnswer *newQuestion = [[QuestionAndAnswer alloc]init];
    newQuestion.question = question;
    newQuestion.answer = answer;
    [self loadDataFromFile];
    [_questions addObject:newQuestion];
    /*
    if ([NSKeyedArchiver archiveRootObject:_questions toFile:_docPath]){
        NSLog(@"Save successful.");
    }else{
        NSLog(@"Save failed.");
    }
    */
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.questions];
    if ([data writeToFile:_docPath atomically:TRUE]){
        NSLog(@"Save successful.");
    }else{
        NSLog(@"Save failed.");
    }
    
}


-(void)setDocPath{
    NSArray *docFolders =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask, YES);
    NSString *docFolderPath = [docFolders lastObject];
    _docPath = [docFolderPath stringByAppendingPathComponent:@"questionsAdv.txt"];
    NSLog(@"Path: %@", _docPath);
}

-(BOOL)loadDataFromFile{
    //NSData *data = [NSData dataWithContentsOfFile:_docPath];
    _questions = [NSKeyedUnarchiver unarchiveObjectWithFile:_docPath];
    if (_questions) {
        NSLog(@"File loaded.");
        return TRUE;
        
    }else{
        NSLog(@"Failed to load file");
        return FALSE;
        
    }
}

@end
