//
//  FirstViewController.m
//  QuestionBank
//
//  Created by Andrew Muncey on 03/02/2013.
//  Copyright (c) 2013 Andrew Muncey. All rights reserved.
//

#import "AnswerQuestionViewController.h"
#import "QuestionsAdvanced.h"
//#import "Questions.h"

@interface AnswerQuestionViewController ()

@end

@implementation AnswerQuestionViewController{
    //with questions.h
    //Questions *_questions;
    QuestionsAdvanced *_questions;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //load a question with questions.h
    //_questions = [[Questions alloc]init];
    
    _questions = [[QuestionsAdvanced alloc]init];
    [self displayNewQuestion];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



-(void)displayNewQuestion{
    
        
    //the code below displays some static text, you should amend ths method so that it loads a random question from an array.
    //I suggest that you store the question and answers as values in a dictionary
    //You can then store the dictionaries in an array
    //the array could be a propery of ths view controller
    //demo code to set the question and answer text
    //self.questionText.text = @"This should be the question text";
    //self.answerText.text = @"This should be the answer text";
    
    
    /* with questions.h
    NSDictionary *questionAndAnswer = [_questions getNextQuestion];
    if (questionAndAnswer) {
        NSString *answer = [questionAndAnswer objectForKey:@"answer"];
        NSString *question = [questionAndAnswer objectForKey:@"question"];
        self.questionText.text = question;
        self.answerText.text = answer;
        
        self.revealAnswerButton.hidden = NO;
        self.answerText.hidden = YES;
    }else {
        self.questionText.text = @"Please add questions first.";
        self.answerText.text = @"Still waiting for questions...";
        
        self.revealAnswerButton.hidden = NO;
        self.answerText.hidden = YES;
    }
    */
    
    //new way
    QuestionAndAnswer *question = [[QuestionAndAnswer alloc]init];
    question = [_questions getNextQuestion];
    if (question) {
        self.questionText.text = question.question;
        self.answerText.text = question.answer;
        
        self.revealAnswerButton.hidden = NO;
        self.answerText.hidden = YES;
    }else {
        self.questionText.text = @"Please add questions first.";
        self.answerText.text = @"Still waiting for questions...";
        
        self.revealAnswerButton.hidden = NO;
        self.answerText.hidden = YES;
    }
    
}


- (IBAction)getNewQuestionButtonPressed:(id)sender {
    [self displayNewQuestion];
}

- (IBAction)revealAnswer:(UIButton *)sender {

    sender.hidden = YES;
    self.answerText.hidden = NO;
}

@end
