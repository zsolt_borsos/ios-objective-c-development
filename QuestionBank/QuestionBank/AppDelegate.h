//
//  AppDelegate.h
//  QuestionBank
//
//  Created by Andrew Muncey on 03/02/2013.
//  Copyright (c) 2013 Andrew Muncey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
