//
//  Questions.m
//  QuestionBank
//
//  Created by Zsolt Borsos on 17/11/2014.
//  Copyright (c) 2014 Andrew Muncey. All rights reserved.
//

#import "Questions.h"

@implementation Questions {
    
    BOOL *_savedDataExists;
    NSMutableArray *_questions;
    NSString *_docPath;
    
}

-(id)init{
    self = [super init];
    if (self) {
        _questions = [[NSMutableArray alloc]init];
        [self setDocPath];
        if ([self loadQuestionsFromFile]) {
            //loaded file
            //send alert maybe?
            NSLog(@"File loaded.");
            NSLog(@"Hello!");
        }else{
            NSLog(@"Failed to load file.");
        }
        
    }
    return self;
}

-(NSDictionary *)getNextQuestion{
    NSMutableDictionary *answer = [NSMutableDictionary dictionary];
    NSInteger randomPos = 0;
    [self loadQuestionsFromFile];
    
    if (_questions.count > 0) {
        randomPos = arc4random_uniform(_questions.count);
        answer = [_questions objectAtIndex:randomPos];
    }else{
        answer = nil;
    }
    
    
    return answer;
}


-(void)saveQuestion:(NSString *)question withAnswer:(NSString *)answer{
    [self loadQuestionsFromFile];
    NSMutableDictionary *currentQuestion = [NSMutableDictionary dictionary];
    [currentQuestion setObject:question forKey:@"question"];
    [currentQuestion setObject:answer forKey:@"answer"];
    [_questions addObject:currentQuestion];
    [self writeToMyFile];
    
}

-(BOOL)isSavedDataExist{
    
    return _savedDataExists;
}

-(void)setDocPath{
    NSArray *docFolders =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask, YES);
    NSString *docFolderPath = [docFolders lastObject];
    _docPath = [docFolderPath stringByAppendingPathComponent:@"questions.plist"];
    NSLog(@"Path: %@", _docPath);
}

-(BOOL)loadQuestionsFromFile {
    _questions = [NSMutableArray arrayWithContentsOfFile:_docPath];
    if (_questions) {
        return _savedDataExists = TRUE;
    }else{
        return _savedDataExists = FALSE;
    }
    
}

-(void)writeToMyFile {
    NSArray *arrayToSave = [[NSArray alloc]initWithArray:_questions];
    
    if ([arrayToSave writeToFile:_docPath atomically:YES]){
        NSLog(@"File saved.");
    }else {
        NSLog(@"Failed to save file.");
    }
}

@end
