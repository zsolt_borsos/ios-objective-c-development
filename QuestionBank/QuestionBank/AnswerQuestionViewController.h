//
//  AnswerQuestionViewController.h
//  QuestionBank
//
//  Created by Andrew Muncey on 03/02/2013.
//  Copyright (c) 2013 Andrew Muncey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnswerQuestionViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *revealAnswerButton;
@property (weak, nonatomic) IBOutlet UITextView *questionText;
@property (weak, nonatomic) IBOutlet UITextView *answerText;
- (IBAction)getNewQuestionButtonPressed:(id)sender;
- (IBAction)revealAnswer:(id)sender;

@end
