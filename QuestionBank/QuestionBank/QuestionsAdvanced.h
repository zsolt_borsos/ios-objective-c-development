//
//  QuestionsAdvanced.h
//  QuestionBank
//
//  Created by Zsolt Borsos on 23/11/2014.
//  Copyright (c) 2014 Andrew Muncey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuestionAndAnswer.h"

@interface QuestionsAdvanced : NSObject <NSCoding>


@property (nonatomic, strong)NSMutableArray *questions;

-(QuestionAndAnswer *)getNextQuestion;
-(void)saveQuestion:(NSString *)question withAnswer:(NSString *)answer;

@end
