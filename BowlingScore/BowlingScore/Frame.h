//
//  Frame.h
//  BowlingScore
//
//  Created by Zsolt Borsos on 26/01/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Frame : NSObject

@property BOOL strike;
@property NSInteger *pins;
@property BOOL done;
@property NSMutableArray *deliveryHistory;
- (instancetype)initWithRounds:(NSInteger *)round andPins:(NSInteger *)pins;
-(BOOL)deliver:(NSInteger)pins;


@end
