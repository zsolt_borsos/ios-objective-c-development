//
//  Frame.m
//  BowlingScore
//
//  Created by Zsolt Borsos on 26/01/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Frame.h"

@implementation Frame{
    NSInteger *_rounds;
}

- (instancetype)initWithRounds:(NSInteger *)round andPins:(NSInteger *)pins;
{
    self = [super init];
    if (self) {
        _done = NO;
        _strike = NO;
        _rounds = round;
        _pins = pins;
        self.deliveryHistory = [[NSMutableArray alloc]init];
    }
    return self;
}

-(BOOL)deliver:(NSInteger)pins{
    
    if (_rounds == 0) {
        return NO;
    }
    _rounds -= 1;
    _pins -= pins;
    [self.deliveryHistory addObject:[NSNumber numberWithInt:pins]];
    
    
    return YES;
    
}
@end
