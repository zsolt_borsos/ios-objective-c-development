//
//  ExternalDisplayViewController.h
//  BingoWithGUI
//
//  Created by Zsolt Borsos on 23/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExternalDisplayViewController : UIViewController


-(void)updateLabel:(NSString *)text;

@property UILabel *label;

@end
