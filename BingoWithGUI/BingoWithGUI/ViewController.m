//
//  ViewController.m
//  BingoWithGUI
//
//  Created by Zsolt Borsos on 21/11/2014.
//  Copyright (c) 2014 ___ZsoltBorsos___. All rights reserved.
//

#import "ViewController.h"
#import "BingoMachine.h"
#import "ExternalDisplayViewController.h"
@import AVFoundation;

@interface ViewController ()

@end

@implementation ViewController{
    BingoMachine *_bingo;
    __weak IBOutlet UIButton *nextNumberButton;
    
    //UIViewController *_vc;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //_bingo = [[BingoMachine alloc]init];
    [self addExternalScreenObservers];
    
    [self setupGrid];
    nextNumberButton.enabled = FALSE;
    
}

-(void)addExternalScreenObservers {
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(handleScreenConnection) name:UIScreenDidConnectNotification object:nil];
    
    [nc addObserver:self selector:@selector(handleScreenDisconnection) name:UIScreenDidDisconnectNotification object:nil];
    
    
}


-(void)handleScreenConnection {
    
    NSLog(@"Extra screen is on!");
    UIScreen *secondScreen = [[UIScreen screens]objectAtIndex:1];
    self.secondWindow = [[UIWindow alloc]initWithFrame:secondScreen.bounds];
    self.secondWindow.screen = secondScreen;
    self.vc = [[ExternalDisplayViewController alloc]init];
    self.vc.view.frame = self.secondWindow.frame;
    
    [self.secondWindow addSubview:self.vc.view];
    //self.secondWindow.rootViewController = self.vc;
    self.secondWindow.hidden = NO;
    
}

-(void)handleScreenDisconnection {


    self.secondWindow.hidden = YES;
    self.vc = nil;
    self.secondWindow = nil;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startNewGame:(UIButton *)sender {
    
    if (_bingo) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Are you sure you want to start a New Game?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alert show];
    }else{
        [self newGame];
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != 0) {
        [self newGame];
    }
    
}

-(void)newGame {
    _bingo = nil;
    _bingo = [[BingoMachine alloc]init];
    _currentNumberLabel.text = @"";
    _progressBar.progress = 0;
    nextNumberButton.enabled = TRUE;
    [nextNumberButton setTitle:@"Start the game" forState: 0];
    for (UIView *label in [_gameplayArea subviews]) {
        label.backgroundColor = [UIColor whiteColor];
    }
    
}


- (IBAction)getNextNumber:(UIButton *)sender {
    NSString *text = [NSString stringWithFormat:@"%ld", (long)[_bingo getNextNumber]];
    [nextNumberButton setTitle:@"Next number" forState: 0];
    int number = text.intValue;
    if (number != 0) {
        _currentNumberLabel.text = text;
        UIView *currentLabel = [_gameplayArea viewWithTag: number];
        currentLabel.backgroundColor = [UIColor redColor];
        [self setProgress];
        [self sayNumber:number];
        
    }else {
        _currentNumberLabel.text = @"End";
        nextNumberButton.enabled = FALSE;
    }
    
}

-(void)sayNumber:(int)number{
    
    //say it in the bingo waaay
    
    //split and then together
    NSString *stringValue = [NSString stringWithFormat:@"%d", number];
    char firstVal = [stringValue characterAtIndex:0];
    char secondVal = [stringValue characterAtIndex:1];
    AVSpeechSynthesizer *synth = [[AVSpeechSynthesizer alloc] init];
    
    AVSpeechUtterance *toSay = [AVSpeechUtterance speechUtteranceWithString:[NSString stringWithFormat:@"%c", firstVal]];
    [synth speakUtterance:toSay];
    
    toSay = [AVSpeechUtterance speechUtteranceWithString:[NSString stringWithFormat:@"%c", secondVal]];
    [synth speakUtterance:toSay];
    
    toSay = [AVSpeechUtterance speechUtteranceWithString:[NSString stringWithFormat:@"%@", stringValue]];
    [synth speakUtterance:toSay];
    
    
}


-(void)setProgress {
    
    int balls = (int)[_bingo ballsInGame];
    int remaining = (int)[_bingo getRemainingBallsCount];
    //calculate 1%
    float percent = balls / 100.0;
    //calculate remaining progress %
    float rate = remaining/percent;
    //progress takes a value between 0-1, so convert the value in that format
    float progress = (100.0 - rate) / 100.0;
    float currentProgress = progress;
    _progressBar.progress = currentProgress;
    
}

-(void)setupGrid{
    int counter = 1;
    int dimension = 70;
    int border = 12;
    //int x = 5;
    //int y = 5;
    for (int i = 0; i < 9; i++) {
       int y = border + (i * dimension);
       int x = border;
        for (int k = 0; k < 10; k++) {
            UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(x, y, dimension, dimension)];
            number.text = [NSString stringWithFormat:@"%i", counter];
            number.font = [UIFont boldSystemFontOfSize:30];
            number.backgroundColor = [UIColor whiteColor];
            number.textAlignment = NSTextAlignmentCenter;
            number.tag = counter;
            [_gameplayArea addSubview:number];
            x += dimension;
            counter++;
        }
    }
   
    /*
    UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(x, y, dimension, dimension)];
    number.text = [NSString stringWithFormat:@"%i", counter];
    number.backgroundColor = [UIColor blackColor];
    [_gameplayArea addSubview:number];
    */
}



@end
