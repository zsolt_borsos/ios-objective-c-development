//
//  ViewController.h
//  BingoWithGUI
//
//  Created by Zsolt Borsos on 21/11/2014.
//  Copyright (c) 2014 ___ZsoltBorsos___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIAlertViewDelegate>
- (IBAction)startNewGame:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UILabel *currentNumberLabel;
- (IBAction)getNextNumber:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *gameplayArea;

@property UIWindow *secondWindow;
@property UIViewController *vc;


@end
