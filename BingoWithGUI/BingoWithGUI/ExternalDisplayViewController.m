//
//  ExternalDisplayViewController.m
//  BingoWithGUI
//
//  Created by Zsolt Borsos on 23/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "ExternalDisplayViewController.h"

@interface ExternalDisplayViewController ()

@end

@implementation ExternalDisplayViewController{
    
    //UILabel *_label;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _label = [[UILabel alloc]initWithFrame:CGRectMake(150 ,150, 200, 100)];
    _label.text = @"Some text for display";
    _label.font = [UIFont boldSystemFontOfSize:30];
    _label.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_label];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)updateLabel:(NSString *)text {
    
    _label.text = text;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
