//
//  ModalViewController.h
//  ProtocolDemo
//
//  Created by Andrew Muncey on 30/08/2013.
//  Copyright (c) 2013 University of Chester. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ModalDelegate <NSObject>

-(void)getMessage:(NSString *)message;

@end

@interface ModalViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *textFieldForData;
- (IBAction)doneButtonPressed:(UIButton *)sender;
@property (weak) id <ModalDelegate> delegate;

@end
