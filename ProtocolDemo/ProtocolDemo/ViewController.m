//
//  ViewController.m
//  ProtocolDemo
//
//  Created by Andrew Muncey on 30/08/2013.
//  Copyright (c) 2013 University of Chester. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getMessage:(NSString *)message {
    
    NSLog(@"message: %@" , message);
    _labelForData.text = message;
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    ModalViewController *modalViewController = (ModalViewController *)segue.destinationViewController;
    modalViewController.delegate = self;
}

@end
