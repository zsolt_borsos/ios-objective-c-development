//
//  ModalViewController.m
//  ProtocolDemo
//
//  Created by Andrew Muncey on 30/08/2013.
//  Copyright (c) 2013 University of Chester. All rights reserved.
//

#import "ModalViewController.h"

@interface ModalViewController ()

@end

@implementation ModalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonPressed:(UIButton *)sender {
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate getMessage:self.textFieldForData.text];
    
    
}



@end
