//
//  ViewController.h
//  ProtocolDemo
//
//  Created by Andrew Muncey on 30/08/2013.
//  Copyright (c) 2013 University of Chester. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModalViewController.h"

@interface ViewController : UIViewController <ModalDelegate>
@property (strong, nonatomic) IBOutlet UILabel *labelForData;

@end
