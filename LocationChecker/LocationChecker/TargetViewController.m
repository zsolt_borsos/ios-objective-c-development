//
//  TargetViewController.m
//  LocationChecker
//
//  Created by Zsolt Borsos on 03/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "TargetViewController.h"

@interface TargetViewController ()

@end

@implementation TargetViewController{
    
    CLLocation *_targetLocation;
    BOOL _updated;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _targetLocation = [[CLLocation alloc]init];
    //_currentLocation = [[CLLocation alloc]init];
    _updated = NO;
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    //UIViewController *controller = [[UIViewController alloc]init];
    //controller = [segue destinationViewController];
    NSLog(@"Seguel calleD!");
    
}
*/

- (IBAction)goingBack:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];

    
}


- (IBAction)targetMyPosition:(id)sender {
    
    _targetLocation = _currentLocation;
    //NSLog(@"Current Loc: lat: %f lon: %f", _currentLocation.coordinate.latitude, _currentLocation.coordinate.longitude);
    //[self delegateTarget];
    [self updateInputTexts];
    
}

-(void)updateInputTexts {
    
    _targetLat.text = [NSString stringWithFormat:@"%f", _targetLocation.coordinate.latitude];
    
    _targetLon.text = [NSString stringWithFormat:@"%f", _targetLocation.coordinate.longitude];
}

- (IBAction)getRandomPosition:(id)sender {
}

- (IBAction)checkLocationValidity:(id)sender {
    double lat, lon;
    
    
    //check lengths
    if (_targetLat.text.length == 0) {
        //_targetLat.backgroundColor =
        _targetLat.placeholder = @"Fill me...";
        return;
    }
    
    if (_targetLon.text.length == 0) {
        _targetLon.placeholder = @"Don't leave me empty!";
        return;
    }
    
    //NSLog(@"intVal: %i",_targetLat.text.intValue);
    
    
    //check for numbers values!? or not?
    //maybe back to numberpad restriction
    
    //needs checking with textfielddelegate probably...
    
   
    lat = [_targetLat.text doubleValue];
    lon = [_targetLon.text doubleValue];
    
    _targetLocation = [[CLLocation alloc]initWithLatitude:lat longitude:lon];
    
    [self delegateTarget];

    
}

-(void)delegateTarget {
    //remove view from screen
    [self dismissViewControllerAnimated:YES completion:nil];
    //send delegate message
    [self.delegate readInputLocation:_targetLocation];
    
}

@end
