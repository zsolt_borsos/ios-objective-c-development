//
//  TargetViewController.h
//  LocationChecker
//
//  Created by Zsolt Borsos on 03/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@protocol targetDelegate <NSObject>

-(void)readInputLocation:(CLLocation *)location;
@optional
-(void)getLocationFromUser:(CLLocation *)location;
@end

@interface TargetViewController : UIViewController

@property(assign, nonatomic) id< targetDelegate > delegate;

@property (weak, nonatomic) IBOutlet UITextField *targetLat;
@property (weak, nonatomic) IBOutlet UITextField *targetLon;
@property (weak, nonatomic) IBOutlet UIButton *addTargetButton;
@property (strong, nonatomic) CLLocation *currentLocation;

- (IBAction)targetMyPosition:(id)sender;
- (IBAction)getRandomPosition:(id)sender;

- (IBAction)checkLocationValidity:(id)sender;


@end
