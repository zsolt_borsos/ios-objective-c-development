//
//  ViewController.m
//  LocationChecker
//
//  Created by Zsolt Borsos on 02/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "ViewController.h"


@interface ViewController (){
    
    CLLocationManager *_locmanager;
    CLLocation *_currentLocation;
    CLLocation *_targetLocation;
    CLLocationCoordinate2D _currentCoords;
    CLLocationCoordinate2D _targetCoords;
    MKRoute *_myRoute;
    NSDate *_lastUpdate;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //load map with current position
    
    _myRoute = [[MKRoute alloc]init];
    
    [self setPositionFieldsToDefault];
    
    _myMapView.delegate = self;
    
    _locmanager = [[CLLocationManager alloc]init];
    
    _locmanager.delegate = self;

    _locmanager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    
    _locmanager.distanceFilter = kCLDistanceFilterNone;
    
    [_locmanager startUpdatingLocation];
    
    [_locmanager requestWhenInUseAuthorization];
    
    _lastUpdate = nil;
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    //maybe dont need this
    //[_locmanager requestAlwaysAuthorization];
    
}


-(void)mapViewDidFinishLoadingMap:(MKMapView *)mapView {
    
    mapView.showsUserLocation = YES;
    //[self updateCoordsFromLocations];
    //MKMapCamera *cam = [MKMapCamera cameraLookingAtCenterCoordinate: _currentCoords fromEyeCoordinate: _currentCoords eyeAltitude: 800];
    //_myMapView.camera = cam;
    
}

-(void)updateCoordsFromLocations {
    
    _currentCoords = CLLocationCoordinate2DMake(_currentLocation.coordinate.latitude, _currentLocation.coordinate.longitude);
    
    _targetCoords = CLLocationCoordinate2DMake(_targetLocation.coordinate.latitude, _targetLocation.coordinate.longitude);
}

-(void)goToLocationCoords:(CLLocationCoordinate2D)coords {
    
    MKMapCamera *cam = [MKMapCamera cameraLookingAtCenterCoordinate: coords fromEyeCoordinate: coords eyeAltitude: 800];
    _myMapView.camera = cam;
    
}


-(void)setPositionFieldsToDefault {
    
    _progressBar.progress = 1;
    _progressBar.progressTintColor = [UIColor redColor];
    _currentLat.text = @"loading...";
    _currentLon.text = @"loading...";
    _targetLat.text = @"-";
    _targetLon.text = @"-";
    _distance.text = @"-";
    _speed.text = @"-";
    _arrival.text = @"-";
    _debugBar.text = @"Waiting for target...";
    
}


//textfield stuff


-(void)readInputLocation:(CLLocation *)location {

    _targetLocation = location;
    [self updateTargetLocationText];
    _debugBar.text = @"Target updated.";
    [self createRoute];
    
}

-(void)calculateDistance {
    
    //CLLocationDistance
    
}


-(void)createRoute {
    
    NSDictionary *dic = [[NSDictionary alloc]init];
    
    MKPlacemark *targetPlace = [[MKPlacemark alloc]initWithCoordinate:_targetCoords addressDictionary:dic];
    
    MKMapItem *targetPoint  = [[MKMapItem alloc]initWithPlacemark:targetPlace];
    
    //MKMapItem *targetPoint = [[MKMapItem alloc]initWithPlacemark:<#(MKPlacemark *)#>];
    
    MKMapPointForCoordinate(_targetCoords);
    
    MKMapItem *myPoint = [MKMapItem mapItemForCurrentLocation];
    
    MKDirectionsRequest *req = [[MKDirectionsRequest alloc]init];
    req.transportType = MKDirectionsTransportTypeAutomobile;
    
    [req setSource: myPoint];
    [req setDestination:targetPoint];
    
    MKDirections *direction = [[MKDirections alloc]initWithRequest:req];
    [direction calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Error calculating route. %@", error);
            _debugBar.text =@"Error calculating route.";
            
        } else {
            // The code doesn't request alternate routes, so add the single calculated route to
            // a previously declared MKRoute property called walkingRoute.
            _debugBar.text = @"Route calculated";
            _myRoute = response.routes[0];
        }
    }];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)updateTargetLocationText {
    
    NSString *lonStr = [NSString stringWithFormat:@"%f",_targetLocation.coordinate.longitude];
    
    NSString *latStr = [NSString stringWithFormat:@"%f",_targetLocation.coordinate.latitude];
    
    _targetLat.text = latStr;
    _targetLon.text = lonStr;
    
    
}

-(void)updateMyLocationText {
    
    NSString *lat = [NSString stringWithFormat:@"%f", _currentLocation.coordinate.latitude];
    _currentLat.text = lat;
    
    NSString *lon = [NSString stringWithFormat:@"%f", _currentLocation.coordinate.longitude];
    _currentLon.text = lon;
    
    //_debugBar.text = [lat stringByAppendingString:lon];
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    _debugBar.text = @"Location services failed.";
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    //NSLog(@"Logging location!");
    if (_lastUpdate == nil) {
        _currentLocation = [locations lastObject];
        [self updateMyLocationText];
        _lastUpdate = [NSDate date];
    }else{
        NSTimeInterval x = _lastUpdate.timeIntervalSinceNow;
        if (x > 5) {
            _currentLocation = [locations lastObject];
            [self updateMyLocationText];
            _lastUpdate = [NSDate date];
            NSLog(@"Updated!");
        }
    }
    
   
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    TargetViewController *tvc = (TargetViewController *)segue.destinationViewController;
    tvc.currentLocation = _currentLocation;
    tvc.delegate = self;
}



- (IBAction)showCurrentPositionOnMap:(id)sender {
    
    [self goToLocationCoords:_currentCoords];
    
}

- (IBAction)showTargetPositionOnMap:(id)sender {
    
    
    NSString *debugString = [NSString stringWithFormat:@"Lon: %f Lat: %f", _targetLocation.coordinate.longitude, _targetLocation.coordinate.latitude];
    
    _debugBar.text =debugString;
    
    [self goToLocationCoords:_targetCoords];
}
@end
