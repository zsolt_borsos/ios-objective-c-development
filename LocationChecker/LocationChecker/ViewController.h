//
//  ViewController.h
//  LocationChecker
//
//  Created by Zsolt Borsos on 02/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "TargetViewController.h"
#import <MapKit/MapKit.h>

@protocol myLocationDelegate <NSObject>

-(void)getMyLocation:(CLLocation *)location;

@end

@interface ViewController : UIViewController <CLLocationManagerDelegate, targetDelegate, MKMapViewDelegate >

//@property(assign, nonatomic) id<MKMapViewDelegate>mapDelegate;
@property(assign, nonatomic) id<myLocationDelegate>locDelegate;
@property(assign, nonatomic) id<CLLocationManagerDelegate>delegate;

@property (weak, nonatomic) IBOutlet UILabel *targetLon;
@property (weak, nonatomic) IBOutlet UILabel *targetLat;

@property (weak, nonatomic) IBOutlet UILabel *currentLon;
@property (weak, nonatomic) IBOutlet UILabel *currentLat;
@property (weak, nonatomic) IBOutlet UILabel *debugBar;
@property (weak, nonatomic) IBOutlet UILabel *distance;
@property (weak, nonatomic) IBOutlet UILabel *speed;
@property (weak, nonatomic) IBOutlet UILabel *arrival;



@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;



@property (weak, nonatomic) IBOutlet MKMapView *myMapView;
- (IBAction)showCurrentPositionOnMap:(id)sender;
- (IBAction)showTargetPositionOnMap:(id)sender;

@end

