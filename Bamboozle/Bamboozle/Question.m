//
//  Question.m
//  Bamboozle
//
//  Created by Zsolt Borsos on 12/11/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import "Question.h"


@implementation Question


-(id)initWithQuestion:(NSString*)question answers:(NSArray *)answers rightAnswer:(int)rightAnswer{
    self = [super init];
    if (self) {
        self.question = [[NSString alloc] initWithString:question];
        self.answers = [[NSArray alloc]initWithArray:answers];
        self.rightAnswer = rightAnswer;
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self.question = [coder decodeObjectForKey:@"question"];
    self.answers = [coder decodeObjectForKey:@"answers"];
    self.rightAnswer = [coder decodeIntForKey:@"rightAnswer"];
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.question forKey:@"question"];
    [aCoder encodeObject:self.answers forKey:@"answers"];
    [aCoder encodeInt:self.rightAnswer forKey:@"rightAnswer"];
    
}

@end
