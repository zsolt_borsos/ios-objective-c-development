//
//  Question.h
//  Bamboozle
//
//  Created by Zsolt Borsos on 12/11/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject <NSCoding>

@property(nonatomic, strong) NSString *question;
@property(nonatomic, strong) NSArray *answers;
@property(nonatomic) int rightAnswer;

-(id)initWithQuestion:(NSString*)question answers:(NSArray *)answers rightAnswer:(int)rightAnswer;

@end
