//
//  QuestionController.h
//  Bamboozle
//
//  Created by Zsolt Borsos on 30/11/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface QuestionController : NSObject

-(Question *)getNextQuestion;


@end
