//
//  QuestionController.m
//  Bamboozle
//
//  Created by Zsolt Borsos on 30/11/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import "QuestionController.h"


@implementation QuestionController{
    
    NSMutableArray *_questions;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _questions = [[NSMutableArray alloc]init];
        [self loadQuestions];
    }
    return self;
}

-(Question *)getNextQuestion {
    int randomPos;
    int count = (int)_questions.count;
    if ( count > 0) {
        randomPos = arc4random_uniform(count-1);
    }else {
        return nil;
    }
    Question *q = _questions[randomPos];
    if (q) {
        [_questions removeObject:q];
        return q;
    }else{
        return nil;
    }
    
    
    
}

-(void)loadQuestions {
    
    Question *q1 = [[Question alloc]initWithQuestion:@"What is 4 + 2?" answers:@[@"5", @"7", @"6", @"8"] rightAnswer:3];
    
    Question *q2 = [[Question alloc]initWithQuestion:@"What is 5 + 3?" answers:@[@"7", @"8", @"9", @"6"] rightAnswer:2];
    Question *q3 = [[Question alloc]initWithQuestion:@"How are you?" answers:@[@"Good!", @"Not good...", @"Cool!", @"Crap..."] rightAnswer:1];
    
    [_questions addObject:q1];
    [_questions addObject:q2];
    [_questions addObject:q3];
    
}

@end
