//
//  MyScene.m
//  Bamboozle
//
//  Created by student on 30/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import "MyScene.h"
#import "QuestionController.h"

@implementation MyScene
{
    SKLabelNode *_rightAnswer;
    QuestionController *_qc;
    SKNode *_masterNode;
}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        _masterNode = [[SKNode alloc]init];
        if (!_qc) {
            _qc = [[QuestionController alloc]init];
        }
        
        [self createLayoutWithQuestion:[_qc getNextQuestion]];
        
    }
    return self;
}

-(void)createLayoutWithQuestion:(Question *)question {
    
    //SKScene *self = [[MyScene ];
    
    self.backgroundColor = [SKColor colorWithRed:0 green:0 blue:0 alpha:1.0];

    [_masterNode removeAllChildren];
    if (_masterNode.parent) {
        [_masterNode removeFromParent];
    }
    if (!question) {
        SKLabelNode *questionLabel = [SKLabelNode labelNodeWithFontNamed:@"Menlo"];
        
        questionLabel.text = @"No more question...";
        return;
    }
    
    
    //_masterNode.name = @"master";
    SKLabelNode *questionLabel = [SKLabelNode labelNodeWithFontNamed:@"Menlo"];
    
    questionLabel.text = question.question;
    
    questionLabel.fontSize = 30;
    questionLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                         CGRectGetMaxY(self.frame)-50);
    [_masterNode addChild:questionLabel];
    
    SKLabelNode *answer1 = [SKLabelNode labelNodeWithFontNamed:@"Menlo"];
    answer1.text = question.answers[0];
    answer1.position = CGPointMake(70, 200);
    answer1.fontColor = [SKColor redColor];
    [_masterNode addChild:answer1];
    
    SKLabelNode *answer2 = [SKLabelNode labelNodeWithFontNamed:@"Menlo"];
    answer2.text = question.answers[1];
    answer2.position = CGPointMake(240, 200);
    answer2.fontColor = [SKColor greenColor];
    [_masterNode addChild:answer2];
    
    SKLabelNode *answer3 = [SKLabelNode labelNodeWithFontNamed:@"Menlo"];
    answer3.text = question.answers[2];
    answer3.position = CGPointMake(70, 100);
    answer3.fontColor = [SKColor yellowColor];
    [_masterNode addChild:answer3];
    
    SKLabelNode *answer4 = [SKLabelNode labelNodeWithFontNamed:@"Menlo"];
    answer4.text = question.answers[3];
    answer4.position = CGPointMake(240, 100);
    answer4.fontColor = [SKColor blueColor];
    [_masterNode addChild:answer4];
    
    switch (question.rightAnswer) {
        case 1:
            _rightAnswer = answer1;
            break;
        case 2:
            _rightAnswer = answer2;
            break;
        case 3:
            _rightAnswer = answer3;
            break;
        case 4:
            _rightAnswer = answer4;
            break;
    }
    
    
    
    [self addChild:_masterNode];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:self];
    
    if ([_rightAnswer containsPoint:touchLocation]) {
        //SKTransition *fade = [SKTransition fadeWithDuration:1];
        [self createLayoutWithQuestion:[_qc getNextQuestion]];
    }else
    {
        NSLog(@"Nope...");
        NSLog(@"Answer: %@", _rightAnswer.text);
    }
    
    
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
